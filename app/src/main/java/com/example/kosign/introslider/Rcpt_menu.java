package com.example.kosign.introslider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kosign.introslider.adapter.AllReceiptAdapter;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L002_RES;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L002_RES_REC;
import com.webcash.sws.log.DevLog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.R.attr.data;

public class Rcpt_menu extends AppCompatActivity {


    private List<Receipt> all_receiptList;
    private AllReceiptAdapter all_adapter;
    private ListView listView;



    public String loadJSONFromAsset(String json_name) {
        String json;
        try {
            InputStream is = this.getAssets().open(json_name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a006_2_receipt);
        all_receiptList = new ArrayList<>();
        listView = (ListView)findViewById(R.id.lv_receipt);
//        ll_noreceipt = (LinearLayout)findViewById(R.id.ll_noreceipt);
//        rl_receipt = (RelativeLayout)findViewById(R.id.rl_receipt);
//        rl_card = (RelativeLayout)findViewById(R.id.rl_card);
//        ll_card_type = (RelativeLayout)findViewById(R.id.ll_cardtype);
//        rl_listview_below_card = (RelativeLayout)findViewById(R.id.rl_listview_below_card);
//
//
//        iv_show_hide_bottom = (ImageView)findViewById(R.id.iv_show_hide_bottom);
//        iv_show_hide_top = (ImageView)findViewById(R.id.iv_show_hide_top);
//        iv_line_below = (ImageView)findViewById(R.id.iv_line_below);
        all_receiptList = new ArrayList<>();
//
//        ArrayList<HashMap<String, String>> formList = new ArrayList<HashMap<String, String>>();
//        Context context = null;

        try{
            JSONObject json = new JSONObject(loadJSONFromAsset("data.json"));
            JSONArray array = new JSONArray();
            array.put(json);

            TX_MYCD_MBL_L002_RES res = new TX_MYCD_MBL_L002_RES(array);
            res.getINQ_NEXT_APV_DT();           // approved date
            res.getINQ_NEXT_APV_SEQ();          // serial no
            res.getINQ_NEXT_APV_TM();           // approved Time
            res.getINQ_NEXT_APV_NO();           // approved no
            res.getINQ_ITRN_SUB_ROWCOUNT();     // number of repetitions (count row)

            TX_MYCD_MBL_L002_RES_REC rec = res.getRECEIPT_LIST();
            for(int i=0;i < rec.getLength();i++){
                rec.move(i);

                Receipt receipt = new Receipt();
                receipt.setCARD_NO(rec.getCARD_NO());
                receipt.setCARD_NO(rec.getCARD_NO());
                receipt.setAPV_DT(rec.getAPV_DT());
                receipt.setAPV_SEQ(rec.getAPV_SEQ());
                receipt.setAPV_TM(rec.getAPV_TM());
                receipt.setAPV_NO(rec.getAPV_NO());
                receipt.setAPV_AMT(rec.getAPV_AMT());
                receipt.setAPV_CAN_YN(rec.getAPV_CAN_YN());
                receipt.setAPV_CAN_DT(rec.getAPV_CAN_DT());
                DevLog.devLog("APV_CAN_DT", rec.getAPV_CAN_DT());
                receipt.setITLM_MMS_CNT(rec.getITLM_MMS_CNT());
                receipt.setSETL_SCHE_DT(rec.getSETL_SCHE_DT());
                receipt.setMEST_NM(rec.getMEST_NM());
                receipt.setMEST_NO(rec.getMEST_NO());
                receipt.setMEST_BIZ_NO(rec.getMEST_BIZ_NO());
                receipt.setRCPT_TX_STS(rec.getRCPT_TX_STS());
                receipt.setRCPT_RFS_STS(rec.getRCPT_RFS_STS());
                receipt.setCARD_NICK_NM(rec.getCARD_NICK_NM());
                receipt.setDAY_NM(rec.getDAY_NM());
                receipt.setTRAN_KIND_CD(rec.getTRAN_KIND_CD());
                receipt.setTRAN_KIND_NM(rec.getTRAN_KIND_NM());


                all_receiptList.add(receipt);
                Log.d("list","List:"+all_receiptList);
            }

            res.getTRAN_CNT();
            res.getNOTI_CNT();


        }catch (Exception e){

            e.printStackTrace();
        }

        all_adapter = new AllReceiptAdapter(this,all_receiptList);
        listView.setAdapter(all_adapter);
//        final ListView listview=(ListView)findViewById(R.id.lv_receipt);
//        AllReceiptAdapter adapter;
//        adapter=new AllReceiptAdapter(this,);

    }


}
