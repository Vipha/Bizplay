package com.example.kosign.introslider;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.kosign.introslider.tran.ComTran;
import com.example.kosign.introslider.tran.ComTran2;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_P007_REQ;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_P007_RES;
import com.example.kosign.introslider.util.XorCrypto;


public class Login_Page extends AppCompatActivity implements View.OnClickListener, ComTran.OnComTranListener {

    private EditText mUserID, mUserPW;
    private ComTran mComtran;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a005_login);
        try{
            mComtran = new ComTran(this, this);
        }catch (Exception e){
            e.printStackTrace();
        }


        initView();


    }

    private void initView() {

        Button btnLogin     =(Button)findViewById(R.id.btn_login);
        mUserID             =(EditText)findViewById(R.id.UserID);
        mUserPW             =(EditText)findViewById(R.id.UserPw);

        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

//        if (TextUtils.isEmpty(mUserID.getText().toString()) || TextUtils.isEmpty(mUserPW.getText().toString())) {
//           setContentView(R.layout.a005_login);
//            Toast.makeText(getApplicationContext(), "Incorrect !!!", Toast.LENGTH_SHORT).show();
//        } else {
//
//        }
        callLogin();


    }

    private void callLogin() {
        try {
            TX_MYCD_MBL_P007_REQ req = new TX_MYCD_MBL_P007_REQ();
            req.setUSER_ID(mUserID.getText().toString().trim());
            req.setENC_GB("0");
            req.setMOBL_CD("5"); // 1:비플법인카드, 4:비플우리카드, 5:NH소호비즈
            req.setUSER_PW(XorCrypto.encoding(mUserPW.getText().toString().trim()));
            req.setSESSION_ID("");
            req.setTOKEN("");

            mComtran.requestData(TX_MYCD_MBL_P007_REQ.TXNO, req.getSendMessage(), false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranResponse(String tranCode, Object object) {
        try {
            TX_MYCD_MBL_P007_RES res = new TX_MYCD_MBL_P007_RES(object);

            res.getUSER_ID("USER_ID");
            res.getUSER_NM("USER_NM");
            res.getUSER_IMG_PATH("USER_IMG_PATH");
            res.getBSNN_NM("BSNN_NM");
            res.getCRTC_PATH("CRTC_PATH");

            Intent intent=new Intent(Login_Page.this,Menu_card.class);
            startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
