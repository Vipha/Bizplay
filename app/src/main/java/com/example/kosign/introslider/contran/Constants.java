package com.example.kosign.introslider.contran;

/**
 * Created by bunna on 0021, 21/06/2016.
 */
public class Constants {

    public static class LoginInfo {
        public static final String USER_ID = "USER_ID";
        public static final String CRTC_PATH = "CRTC_PATH";
        public static final String USER_IMG_PATH = "USER_IMG_PATH";
        public static final String BSNN_NM = "BSNN_NM";
        public static final String USER_NM = "USER_NM";
        public static final String Auto_LOGIN = "AUTO_LOGIN";
        public static final String USER_PWD = "USER_PWD";

    }

    public static class ReceiptInfo {
        public static boolean WILL_RELOAD = true;

        public static boolean RELOAD_RECEIPT = false;

        public static boolean SHOW_SEND_SUCCESS_SNACKBAR = false;    // send success

        public static boolean SHOW_UPDATE_SNACKBAR = false;     // update success

        public static boolean SHOW_MOVE_SUCCESS_SNACKBAR = false;        // move success

        public static boolean SHOW_CANCEL_SUCESS_SNACKBAR = false; // cancel success

        public static String HIDE_HAND_TUTORIAL = "false";
    }

    public static class MycardInfo {
        public static boolean RELOAD_CARD = false;

        public static boolean ADD_NEW_CARD = false;

        public static String PREVIOUS_USER = "DEMOZ";

        public static String RELOAD_CARD_RESUME = "RELOAD_CARD_RESUME";
    }

    public static class MainInfo {
        public static String selected_card_number = "";
        public static String selected_card_name = "";
        public static String selected_card_corp_cd = "";
        public static String selected_card_org_name = "";
        public static String selected_card_manager_name = "Manager Name";

        public static boolean is_first_load_card = true;

        public static boolean is_from_gcm = false;

        public static String selected_tab = "01";



        public static String APP_SELECTED_CARD_NUMBER = "";


        public static boolean LOAD_PUSH = false;   // for Push while app is running

        public static boolean PUSH_GCM = false;    // for check if app is run from Push or normal click
    }

    public static class NotificationSetting{
        public static final String IS_MAIN_ON = "IS_MAIN_ON";               //푸시알림
        public static final String IS_REAL_TIME_ON = "IS_REAL_TIME_ON";     //실시간 승인내역 알림
        public static final String IS_LIMITED_DT_ON = "IS_LIMITED_DT_ON";   //한도 초기화 일자 알림
        public static final String IS_TIME_OUT_ON = "IS_TIME_OUT_ON";       //알림 제한 시간 설정
        public static final String TIME_OUT_START = "TIME_OUT_START";
        public static final String TIME_OUT_END = "TIME_OUT_END";
    }

    public static class RegisterCardCom {
        public static final String SELECTED_ITEM = "SELECTED_ITEM";
        public static final String SELECTED_CARD_NO = "SELECTED_CARD_NO";
        public static final String IS_DELETED = "IS_DELETED";
    }
}
