package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_MYCD_MBL_P007_RES extends TxMessage {

    public static final String TXNO =   "MYCD_MBL_P007"; //User Login (Password Encryption) respond data
    private TX_MYCD_MBL_P007_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_P007_RES(Object obj) throws Exception{

        mTxKeyData=new TX_MYCD_MBL_P007_RES_DATA();
        super.initRecvMessage(obj);
    }

    public String getUSER_ID(String get_user_id) throws JSONException,Exception {
        return getString(mTxKeyData.USER_ID);
    }

    public String getUSER_NM(String get_user_nm)throws JSONException,Exception {
        return getString(mTxKeyData.USER_NM);
    }

    public String getUSER_IMG_PATH(String get_user_img)throws JSONException,Exception {
        return getString(mTxKeyData.USER_IMG_PATH);
    }

    public String getBSNN_NM(String getbsnn) throws JSONException,Exception{
        return getString(mTxKeyData.BSNN_NM);
    }

    public String getCRTC_PATH(String getcrtc)throws JSONException,Exception {
        return getString(mTxKeyData.CRTC_PATH);
    }


    private class TX_MYCD_MBL_P007_RES_DATA {
        private String USER_ID ="USER_ID";
        private String USER_NM  =   "USER_NM";

        private String USER_IMG_PATH    =   "USER_IMG_PATH";
        private String BSNN_NM  ="BSNN_NM";
        private String CRTC_PATH    =   "CRTC_PATH";
    }
}
