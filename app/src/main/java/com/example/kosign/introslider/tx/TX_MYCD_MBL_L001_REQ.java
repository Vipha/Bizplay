package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * 보유카드목록조회, Card List
 * Created by BUNNA on 0001, 01/07/2016.
 */
public class TX_MYCD_MBL_L001_REQ extends TxMessage {

    public static final String TXNO= "MYCD_MBL_L001";	// 전문 구분 코드

    private TX_MYCD_MBL_L001_REQ_DATA mTxKeyData;			// 전문명세 필드

    public TX_MYCD_MBL_L001_REQ() throws Exception{

        mTxKeyData = new TX_MYCD_MBL_L001_REQ_DATA();
        super.initSendMessage();

    }

    /**
     * 조회일자
     * @param value
     * @throws JSONException
     */
    public void setINQ_DT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.INQ_DT, value);
    }


    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L001_REQ_DATA {
        private String INQ_DT		= 	"INQ_DT";				//조회일자
    }

}
