package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

/*
 * Created by User on 14-Feb-17.
 */

public class TX_MYCD_MBL_L007_RES extends TxMessage {
    private TX_MYCD_MBL_L007_RES_DATA mTxKeyData;

    public TX_MYCD_MBL_L007_RES(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L007_RES_DATA();
        super.initRecvMessage(object);
    }

    public TX_MYCD_MBL_L007_RES_REC getBP_EMPL_REC() throws Exception {
        return new TX_MYCD_MBL_L007_RES_REC(getRecord(mTxKeyData.BP_EMPL_REC));
    }

    private class TX_MYCD_MBL_L007_RES_DATA{
        private String BP_EMPL_REC = "BP_EMPL_REC";
    }
}
