package com.example.kosign.introslider.tran;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.util.Log;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.kosign.introslider.Login_Page;
import com.example.kosign.introslider.Menu_Bar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;


import com.webcash.sws.log.DevLog;
import com.webcash.sws.network.VolleyNetwork;
import com.webcash.sws.network.internal.NetworkErrorCode;
import com.webcash.sws.network.internal.OnNetworkListener;
import com.webcash.sws.network.tx.JSONHelper;
import com.webcash.sws.pref.MemoryPreferenceDelegator;
import com.webcash.sws.pref.PreferenceDelegator;
import com.webcash.sws.util.ComUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;

public class ComTran implements OnNetworkListener {
    public String mUserAgent = "Mozilla/5.0 (Linux; U; Android 2.1; en-us; sdk Build/ERD79) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17";

    private Context mContext;                           // Activity Context
    private OnComTranListener mTranListener;            // UI단 통신 결과 Listener
    private VolleyNetwork mVolleyNetwork;               // 통신 Class
    private GoogleCloudMessaging mGCM = null;
    private boolean mIsDialog;
//    private ComLoading mLoading;
    private String errmsg;

    public ComTran(Context context , OnComTranListener listener) {

        mTranListener = listener;
        mContext = context;
//        mLoading = new ComLoading(context);
        mVolleyNetwork = new VolleyNetwork(mContext , this);
    }

    public void requestData(String tranCd, HashMap<String, Object> requestData) throws JSONException {
        requestData(tranCd, requestData, true);
    }

    /*
     * MG 전문 호출
     * @param tranCd
     */
    public void requestData(String tranCd , String url) {

        ConnectivityManager connectManager = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        //+ 인터넷 연결 상태
        if(connectManager.getActiveNetworkInfo() == null) {
            //+네트워크 연결 오류
            onErrorData(tranCd, NetworkErrorCode.TRNS_ERRCD_INTERNET , null);
            return;
        }
        mVolleyNetwork.requestVolleyNetwork(tranCd, false, url, false);
    }

    /*
     *  전문송신 - JSON Data
     * @param tranCd
     * @param tran_req_data
     */
    @SuppressWarnings("deprecation")
    public void requestData(String tranCd, HashMap<String, Object> tran_req_data, boolean isdialog) throws JSONException {

        String bizURL="http://172.20.50.239:28080/CardAPI.do";
        Log.d("Testing","bizURL:"+ bizURL);
//        String bizURL = MemoryPreferenceDelegator.getInstance().get("BIZ_URL");
//       String bizURL =
//        if(bizURL.equals("")) {
//            //+ 전문 page url 이 없는경우 리턴
//            return;
//        }

        mIsDialog = isdialog;

        if(isdialog) {
//            showProgressDialog();
//            mLoading.showProgressDialog();
        }

        // 네트워크 연결 확인
//        if(ComUtil.getNetworkStatus((Activity)mContext)) {

            JSONObject jsonObj =  (JSONObject) JSONHelper.toJSON(tran_req_data);

            DevLog.devLog("nryoo", "jsonObj ::    " + jsonObj.toString());

            JSONObject jobjectInput = new JSONObject();

            jobjectInput.put(ComTranCode.CNTS_CRTS_KEY_CODE  , "");
            jobjectInput.put(ComTranCode.KEY_TRAN_CODE  , tranCd);
            jobjectInput.put(ComTranCode.KEY_REQ_DATA   , jsonObj);

            DevLog.devLog("nryoo", "requestData ::    " + jobjectInput.toString());

            //+header
            HashMap<String, String> headers = new HashMap<>();
            headers.put("charset", "UTF-8");
            headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            headers.put("User-Agent", mUserAgent);
            mVolleyNetwork.setComHeaders(headers);

            try {
                DevLog.devLog("nryoo", "send :: " + bizURL + "?JSONData=" + URLEncoder.encode(jobjectInput.toString()));
                Log.d("test","data_res_Volley: ");
                mVolleyNetwork.requestVolleyNetwork(tranCd, false, bizURL + "?JSONData=" + URLEncoder.encode(jobjectInput.toString(), "UTF-8"), false);
            } catch (Exception e) {
                e.printStackTrace();
            }
//        }else {
//            //+네트워크 연결 오류
//            onErrorData(tranCd, NetworkErrorCode.TRNS_ERRCD_INTERNET, null);
//        }
    }

    public void requestPost(final String tranCd, HashMap<String, Object> tran_req_data, boolean isdialog) throws JSONException {

        String bizURL = MemoryPreferenceDelegator.getInstance().get("BIZ_URL");
        if(bizURL.equals("")) {
            //+ 전문 page url 이 없는경우 리턴
            return;
        }

        mIsDialog = isdialog;

        if(isdialog) {
//            showProgressDialog();
//            mLoading.showProgressDialog();
        }

        // 네트워크 연결 확인
        if(ComUtil.getNetworkStatus((Activity)mContext)) {

            JSONObject jsonObj =  (JSONObject) JSONHelper.toJSON(tran_req_data);

            DevLog.devLog("nryoo", "jsonObj ::    " + jsonObj.toString());

            JSONObject jobjectInput = new JSONObject();

            jobjectInput.put(ComTranCode.CNTS_CRTS_KEY_CODE  , "");
            jobjectInput.put(ComTranCode.KEY_TRAN_CODE  , tranCd);
            jobjectInput.put(ComTranCode.KEY_REQ_DATA   , jsonObj);

            DevLog.devLog("nryoo", "requestData ::    " + jobjectInput.toString());

            //+header
            HashMap<String, String> headers = new HashMap<>();
            headers.put("charset", "UTF-8");
            headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            headers.put("User-Agent", mUserAgent);
            mVolleyNetwork.setComHeaders(headers);

            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("JSONData", URLEncoder.encode(jobjectInput.toString(), "UTF-8"));
                mVolleyNetwork.requestVolleyNetwork(tranCd, true, bizURL ,params, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            //+네트워크 연결 오류
            onErrorData(tranCd, NetworkErrorCode.TRNS_ERRCD_INTERNET, null);
        }
    }

    @Override
    public void onNetworkResponse(final String tranCode, Object object) {

        //+ 전문 응답부
        if(mTranListener == null){
            return;
        }

        JSONObject jsonOutput;
        JSONArray jarrayResData   = null;

        try {

            jsonOutput = new JSONObject(URLDecoder.decode(object.toString().replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8"));
            DevLog.devLog("nryoo", "onGtinNetworkResponse jsonOutput::" + jsonOutput.toString());


            if(tranCode.equals("MG_DATA")){
                if(!jsonOutput.isNull(ComTranCode.KEY_RES_DATA)) {
                    jsonOutput = jsonOutput.getJSONObject(ComTranCode.KEY_RES_DATA);
                    if(!jsonOutput.isNull(ComTranCode.KEY_TRAN_RES_DATA)) {
                        jarrayResData = jsonOutput.getJSONArray(ComTranCode.KEY_TRAN_RES_DATA);
                    }
                } else {
                    onErrorData(tranCode, NetworkErrorCode.TRNS_ERRCD_PAGEERR, jsonOutput);
                    return;
                }
            } else {
                /* 응답 값 */
                if(!jsonOutput.isNull(ComTranCode.KEY_RSLT_CD)){
                    //* 오류발생 전문 코드값 처리
                    String resultErrorCd = jsonOutput.getString(ComTranCode.KEY_RSLT_CD);
                    if(!resultErrorCd.equals("0000")) {
//                        if(!tranCode.equals(TX_MYCD_MBL_R001_REQ.TXNO)) {
//                            if (tranCode.equals(TX_MYCD_MBL_C005_REQ.TXNO)) {
//                                // catch error 등록 & 저장
//                                mLoading.dismissProgressDialog(); //+ 통신 Dialog Dismiss
//                                mTranListener.onTranError(tranCode,jsonOutput);
//                                return;
//                            }else {
//                                onErrorData(tranCode, resultErrorCd, jsonOutput);
//                                return;
//                            }
//                        } else {
//                            mLoading.dismissProgressDialog();
//                            mTranListener.onTranError(tranCode,jsonOutput);
//                            return;
//                        }

                    }
                }

                if(!jsonOutput.isNull(ComTranCode.KEY_RES_DATA)) {
                    jarrayResData = new JSONArray();
                    jarrayResData.put(jsonOutput.getJSONObject(ComTranCode.KEY_RES_DATA));
                }
            }

            //+ 통신 Dialog Dismiss
//            dismissProgressDialog();
//            mLoading.dismissProgressDialog();
            Log.d("test","data_res_com: ");
            mTranListener.onTranResponse(tranCode, jarrayResData);
            return;



        } catch (JSONException e1) {
            e1.printStackTrace();
            //onNetworkError(tranCode, NetworkErrorCode.TRNS_ERRCD_PASER );
            onErrorData(tranCode, NetworkErrorCode.TRNS_ERRCD_PASER, null);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //onNetworkError(tranCode, NetworkErrorCode.TRNS_ERRCD_PASER);
            onErrorData(tranCode, NetworkErrorCode.TRNS_ERRCD_PASER, null);
        }
        catch (Exception e) {
            e.printStackTrace();
            onErrorData(tranCode, NetworkErrorCode.APP_ERRCD_UNKNOWN, null);
            //onNetworkError(tranCode, NetworkErrorCode.APP_ERRCD_UNKNOWN);
        }
    }

    @Override
    public void onNetworkError(String tranCode, Object object) {

        String errorStr;
        //+ 전문 오류
        if(object instanceof VolleyError){
            VolleyError error = (VolleyError)object;
            DevLog.devLog("nryoo", "onNetworkError VolleyError::" + error.toString());
            errorStr = error.toString();
        }else         {
            DevLog.devLog("nryoo", "onNetworkError jsonOutput::" + object.toString());
            errorStr =  object.toString();
        }
        onErrorData(tranCode, NetworkErrorCode.TRNS_ERRCD_PAGEERR, errorStr);
        /*dismissProgressDialog();
        mTranListener.onTranError(tranCode, errorStr);*/

    }


    /*
     * 통신중 에러 처리
     * @param tranCode
     * @param errcd
     * @param error
     */
    private void onErrorData(final String tranCode, final String errcd, Object error) {

        try {
//            mLoading.dismissProgressDialog();

            errmsg = "";
            if (errcd.equals(NetworkErrorCode.TRNS_ERRCD_INTERNET)) {

                errmsg = errmsg + "인터넷 연결이 불안정합니다.\n연결 후 이용하시기 바랍니다.";
//                error = errmsg;
            } else if (errcd.equals(NetworkErrorCode.TRNS_ERRCD_MAKE)
                    || errcd.equals(NetworkErrorCode.TRNS_ERRCD_PASER)
                    || errcd.equals(NetworkErrorCode.APP_ERRCD_UNKNOWN)) {
                // 공통에서 전문발송 데이타를 만드는중에러
                // 전문결과 받은후 공통에서 json 파싱중 에러 (전문 형식 잘못됨)
                errmsg = errmsg + "처리중 오류가 발생하였습니다.";
//                error = errmsg;
            } else if (errcd.equals(NetworkErrorCode.TRNS_ERRCD_PAGEERR)) {
//            return;
                errmsg = errmsg + "통신 상태가 불안정합니다.\n잠시 후 이용하시기 바랍니다.";
//                error = errmsg;
            } else if (errcd.equals("M_2001") || errcd.equals("M_2002")) {
                if (error instanceof JSONObject) {
                    JSONObject jsonObj = (JSONObject) error;
                    if (jsonObj.has(ComTranCode.KEY_RSLT_MSG)) {

                        try {
                            errmsg = jsonObj.getString(ComTranCode.KEY_RSLT_MSG);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    errmsg = errmsg + error.toString();
                }

            } else {

                if (error instanceof JSONObject) {
                    JSONObject jsonObj = (JSONObject) error;
                    if (jsonObj.has(ComTranCode.KEY_RSLT_MSG)) {

                        try {
                            errmsg = jsonObj.getString(ComTranCode.KEY_RSLT_MSG);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    errmsg = errmsg + error.toString();
                }
            }

            final String ErrorTranCode;
            final Object objError = error;
            ErrorTranCode = tranCode;

            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!((Activity) mContext).isFinishing()) {

                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(mContext);
                        builder.setTitle("note");
                        builder.setMessage(errmsg);
                        builder.setCancelable(false);
                        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (mTranListener == null) {
                                    return;
                                }
                                // 세션이 종료되었습니다.
//                                if (errcd.equals("M_2001") || errcd.equals("M_2002")) {
//                                    DevLog.devLog("ComTran", "Session was end!");
//
//                                    Intent intent;
//                                    String check_auto_login = PreferenceDelegator.getInstance(mContext).get(Constants.LoginInfo.Auto_LOGIN);
//                                    if (check_auto_login.equals("") || check_auto_login.equals("false")) {
//                                        intent = new Intent(mContext, LoginActivity.class);
//                                    } else {
//                                        intent = new Intent(mContext, IntroActivity.class);
//                                    }
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                                    mContext.startActivity(intent);
//                                    ((Activity) mContext).finish();
//                                } else if (errcd.equals("1008") || errcd.equals("5510")) {
//                                    PreferenceDelegator.getInstance(mContext).put(Constants.LoginInfo.Auto_LOGIN, "false");
//                                    Intent intent = new Intent(mContext, LoginActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                                    mContext.startActivity(intent);
//                                    ((Activity) mContext).finish();
//                                } else {
//                                    mTranListener.onTranError(ErrorTranCode, objError);
//                                }
                            }
                        }).show();
                    }
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     *  쿠키값 설정
     */
//    public void syncCookieManager() {
//        String domainURL = MemoryPreferenceDelegator.getInstance().get("BIZ_URL");
//        mVolleyNetwork.syncSessionWithWebView(domainURL);
//    }

    /**
     * 세션정보를 초기화한다.
     */
//    public void sessionKill() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            CookieManager.getInstance().removeSessionCookies(null);
//            CookieManager.getInstance().removeAllCookies(null);
//        }else {
//
//            CookieManager.getInstance().removeExpiredCookie();
//            CookieManager.getInstance().removeSessionCookie();
//            CookieSyncManager.getInstance().resetSync();
//        }
//
////		Conf.LAST_TRAN_TIME = Conf.AUTO_LOGOUT_TIME;
//
//    }

    /**
     *  통신 처리 UI단 전달 Listener
     */
    public interface OnComTranListener {
        void onTranResponse(String tranCode, Object object) throws Exception;
        void onTranError(String tranCode, Object object);
    }

    private class ComTranCode {

        //+요청
        public static final String KEY_REQ_DATA 				= "REQ_DATA";	// 전문 요청 DATA
        public static final String KEY_TRAN_CODE 				= "TRAN_NO";	// 전문 요청 CODE
        public static final String CNTS_CRTS_KEY_CODE 			= "CNTS_CRTS_KEY";	// 전문 요청 CODE

        //+응답
        public static final String KEY_RSLT_CD 					= "RSLT_CD";	// 결과코드
        public static final String KEY_RSLT_MSG					= "RSLT_MSG";	// 결과메세지
        public static final String KEY_RES_DATA 				= "RESP_DATA";	// 응답데이타
        public static final String KEY_TRAN_RES_DATA 			= "_tran_res_data";	// 정상 응답 전문
    }

    /**
     * Push register
     */
    @SuppressWarnings("deprecation")
    public void requestPushRegister(final String tranCd, boolean isdialog){

        if (isdialog) {
//            mLoading.showProgressDialog();
        }

        ConnectivityManager connectManager = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        //* 인터넷 연결 상태
        if(connectManager.getActiveNetworkInfo() == null){
            //* 네트워크 연결 오류
            onErrorData(tranCd, NetworkErrorCode.TRNS_ERRCD_INTERNET, null);
            return;
        }

        /**
         * GCM 정보 설정
         */
        //* 1. Google Play Services 사용 가능 여부 체크
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
        if(resultCode != ConnectionResult.SUCCESS) {

//            mLoading.dismissProgressDialog();

            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) mContext, 0);
            if (dialog != null) {
                //This dialog will help the user update to the latest GooglePlayServices
                dialog.show();
            }
//            onErrorData(tranCd, NetworkErrorCode.PUSH_ERRCD_UNKNOWN_GCM, null);
            return;
        }
//                String regId = FirebaseInstanceId.getInstance().getToken();
//                if(!regId.equals("")) {
//                    try {
//                        TX_MYCD_PUSH_REG_REQ req = new TX_MYCD_PUSH_REG_REQ();
//                        req.setPUSHSERVER_KIND("GCM");
//                        req.setAPPID("com.nh.bizcard");
//                        req.setPUSH_ID(regId);
//                        req.setMODEL_NAME(Build.MODEL);
//                        req.setOS(Build.VERSION.RELEASE);
//                        req.setCOMPANY_ID("bizplay");
//                        req.setDEVICE_ID(Utils.createUUID(mContext));
//                        req.setRELATION_KEY(PreferenceDelegator.getInstance(mContext).get(Constants.LoginInfo.USER_ID));
//                        req.setRETRANS_YN("N");
//                        req.setREMARK("Android");
//                        requestData(tranCd,req.getSendMessage(),false);
//                    } catch(Exception e) {
//                        e.printStackTrace();
//                        onErrorData(tranCd, NetworkErrorCode.PUSH_ERRCD_UNKNOWN, null);
//                    }
//                } else {
//                    onErrorData(tranCd, NetworkErrorCode.PUSH_ERRCD_UNKNOWN, null);
//                }
    }

}
