package com.example.kosign.introslider;
/*
 * Created by KOSIGN on 10/10/2017.
 */
public class Receipt {

    private String CARD_NO;
    private String APV_DT;
    private String APV_SEQ;
    private String APV_TM;
    private String APV_NO;
    private String APV_AMT;
    private String APV_CAN_YN;
    private String APV_CAN_DT;
    private String ITLM_MMS_CNT;
    private String SETL_SCHE_DT;
    private String MEST_NM;
    private String MEST_NO;
    private String MEST_BIZ_NO;
    private String RCPT_TX_STS;
    private String RCPT_RFS_STS;
    private String CARD_NICK_NM;
    private String DAY_NM;
    private String TRAN_KIND_CD;
    private String TRAN_KIND_NM;
    private String CARD_ORG_NM;
    private String CARD_CORP_CD;

    public String getCARD_CORP_CD() {
        return CARD_CORP_CD;
    }

    public void setCARD_CORP_CD(String CARD_CORP_CD) {
        this.CARD_CORP_CD = CARD_CORP_CD;
    }

    public String getCARD_ORG_NM() {
        return CARD_ORG_NM;
    }

    public void setCARD_ORG_NM(String CARD_ORG_NM) {
        this.CARD_ORG_NM = CARD_ORG_NM;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getAPV_DT() {
        return APV_DT;
    }

    public void setAPV_DT(String APV_DT) {
        this.APV_DT = APV_DT;
    }

    public String getAPV_SEQ() {
        return APV_SEQ;
    }

    public void setAPV_SEQ(String APV_SEQ) {
        this.APV_SEQ = APV_SEQ;
    }

    public String getAPV_NO() {
        return APV_NO;
    }

    public void setAPV_NO(String APV_NO) {
        this.APV_NO = APV_NO;
    }

    public String getAPV_TM() {
        return APV_TM;
    }

    public void setAPV_TM(String APV_TM) {
        this.APV_TM = APV_TM;
    }

    public String getAPV_AMT() {
        return APV_AMT;
    }

    public void setAPV_AMT(String APV_AMT) {
        this.APV_AMT = APV_AMT;
    }

    public String getAPV_CAN_YN() {
        return APV_CAN_YN;
    }

    public void setAPV_CAN_YN(String APV_CAN_YN) {
        this.APV_CAN_YN = APV_CAN_YN;
    }

    public String getAPV_CAN_DT() {
        return APV_CAN_DT;
    }

    public void setAPV_CAN_DT(String APV_CAN_DT) {
        this.APV_CAN_DT = APV_CAN_DT;
    }

    public String getITLM_MMS_CNT() {
        return ITLM_MMS_CNT;
    }

    public void setITLM_MMS_CNT(String ITLM_MMS_CNT) {
        this.ITLM_MMS_CNT = ITLM_MMS_CNT;
    }

    public String getSETL_SCHE_DT() {
        return SETL_SCHE_DT;
    }

    public void setSETL_SCHE_DT(String SETL_SCHE_DT) {
        this.SETL_SCHE_DT = SETL_SCHE_DT;
    }

    public String getMEST_NM() {
        return MEST_NM;
    }

    public void setMEST_NM(String MEST_NM) {
        this.MEST_NM = MEST_NM;
    }

    public String getMEST_NO() {
        return MEST_NO;
    }

    public void setMEST_NO(String MEST_NO) {
        this.MEST_NO = MEST_NO;
    }

    public String getMEST_BIZ_NO() {
        return MEST_BIZ_NO;
    }

    public void setMEST_BIZ_NO(String MEST_BIZ_NO) {
        this.MEST_BIZ_NO = MEST_BIZ_NO;
    }

    public String getRCPT_TX_STS() {
        return RCPT_TX_STS;
    }

    public void setRCPT_TX_STS(String RCPT_TX_STS) {
        this.RCPT_TX_STS = RCPT_TX_STS;
    }

    public String getRCPT_RFS_STS() {
        return RCPT_RFS_STS;
    }

    public void setRCPT_RFS_STS(String RCPT_RFS_STS) {
        this.RCPT_RFS_STS = RCPT_RFS_STS;
    }

    public String getCARD_NICK_NM() {
        return CARD_NICK_NM;
    }

    public void setCARD_NICK_NM(String CARD_NICK_NM) {
        this.CARD_NICK_NM = CARD_NICK_NM;
    }

    public String getDAY_NM() {
        return DAY_NM;
    }

    public void setDAY_NM(String DAY_NM) {
        this.DAY_NM = DAY_NM;
    }

    public String getTRAN_KIND_CD() {
        return TRAN_KIND_CD;
    }

    public void setTRAN_KIND_CD(String TRAN_KIND_CD) {
        this.TRAN_KIND_CD = TRAN_KIND_CD;
    }

    public String getTRAN_KIND_NM() {
        return TRAN_KIND_NM;
    }

    public void setTRAN_KIND_NM(String TRAN_KIND_NM) {
        this.TRAN_KIND_NM = TRAN_KIND_NM;
    }
}

