package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/*
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_IBK_MBL_R003_RES_RCPT_IMG_REC extends TxMessage {

    private String RCPT_IMG_URL =   "RCPT_IMG_URL";

    public TX_IBK_MBL_R003_RES_RCPT_IMG_REC(Object obj) throws Exception {
        super.initRecvMessage(obj);
    }

    public String getRCPT_IMG_URL() throws JSONException{
        return getString(RCPT_IMG_URL);
    }
}
