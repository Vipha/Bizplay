package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/*
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_IBK_MBL_R003_RES_RCPT_RFS_REC extends TxMessage {

    private String RCPT_RFS_HPNO    =   "RCPT_RFS_HPNO";
    private String RCPT_RFS_NM      =   "RCPT_RFS_NM";
    private String RCPT_RFS_USER_NO =   "RCPT_RFS_USER_NO";

    public TX_IBK_MBL_R003_RES_RCPT_RFS_REC(Object obj)throws Exception{
        super.initRecvMessage(obj);
    }

    public String getRCPT_RFS_HPNO() throws JSONException {
        return getString(RCPT_RFS_HPNO);
    }

    public String getRCPT_RFS_NM() throws JSONException {
        return getString(RCPT_RFS_NM);
    }

    public String getRCPT_RFS_USER_NO() throws JSONException {
        return getString(RCPT_RFS_USER_NO);
    }
}
