package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/*
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_IBK_MBL_R003_RES_RCPT_USER_REC extends TxMessage{

    private String  RCPT_USER_HPNO    =     "RCPT_USER_HPNO";
    private String  RCPT_USER_NM      =     "RCPT_USER_NM";
    private String  RCPT_USER_NO      =     "RCPT_USER_NO";
    private String  RCPT_USER_BSNN_NM =     "RCPT_USER_BSNN_NM";
    private String  RCPT_USER_PSN_NM  =     "RCPT_USER_PSN_NM";
    private String  RCPT_USER_PSN_IMG =     "RCPT_USER_PSN_IMG";

    public TX_IBK_MBL_R003_RES_RCPT_USER_REC(Object obj) throws Exception {
        super.initRecvMessage(obj);
    }


    public String getRCPT_USER_HPNO() throws JSONException {
        return getString(RCPT_USER_HPNO);
    }

    public String getRCPT_USER_NM() throws JSONException{
        return getString(RCPT_USER_NM);
    }

    public String getRCPT_USER_NO()throws JSONException {
        return getString(RCPT_USER_NO);
    }

    public String getRCPT_USER_BSNN_NM() throws JSONException{
        return getString(RCPT_USER_BSNN_NM);
    }

    public String getRCPT_USER_PSN_NM()throws JSONException {
        return getString(RCPT_USER_PSN_NM);
    }

    public String getRCPT_USER_PSN_IMG()throws JSONException {
        return getString(RCPT_USER_PSN_IMG);
    }


}
