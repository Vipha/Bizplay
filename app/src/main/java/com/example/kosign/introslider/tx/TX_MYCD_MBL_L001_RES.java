package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * card list response
 * Created by Bunna on 0001, 01/07/2016.
 */
public class TX_MYCD_MBL_L001_RES extends TxMessage {

    private  TX_MYCD_MBL_L001_RES_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_L001_RES(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L001_RES_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 카드등록건수 (건수가 "0"인 경우 등록카드 없는 안내 페이지 출력 )
     * @throws Exception
     */
    public String getCARD_REG_CNT() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_REG_CNT);
    }

    /**
     * 법인카드 관리자명 (ex) "홍길동 외 1명" )
     * @throws Exception
     */
    public String getCARD_MAGR_NM() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_MAGR_NM);
    }

    /**
     * 반복부
     * @throws Exception
     */
    public TX_MYCD_MBL_L001_RES_REC getCARD_LIST() throws JSONException,Exception{
        return new TX_MYCD_MBL_L001_RES_REC(getRecord(mTxKeyData.REC));
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L001_RES_DATA {
        private String CARD_REG_CNT		= 	"CARD_REG_CNT";				//카드등록건수 (건수가 "0"인 경우 등록카드 없는 안내 페이지 출력 )
        private String CARD_MAGR_NM		= 	"CARD_MAGR_NM";				//법인카드 관리자명 (ex) "홍길동 외 1명" )
        private String REC		        = 	"REC";				        //반복부
    }

}
