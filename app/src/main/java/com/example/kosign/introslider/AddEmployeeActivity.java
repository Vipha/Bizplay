package com.example.kosign.introslider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import com.example.kosign.introslider.adapter.EmployeeAdapter;
import com.example.kosign.introslider.carouselview.MyPageItem;
import com.example.kosign.introslider.tran.ComTran;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L007_REQ;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L007_RES;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L007_RES_REC;
import com.example.kosign.introslider.util.SoundSearcher;
import com.webcash.sws.log.DevLog;

import java.util.ArrayList;
import java.util.Locale;

import item.Employee;

public class AddEmployeeActivity extends AppCompatActivity implements ComTran.OnComTranListener{

    private ListView listViewEmployee,listViewSearch;
    private EmployeeAdapter employeeAdapter,searchAdapter;
    private ArrayList<Employee> listEmployee,listSearch;
    private EditText etSearch;
    private ComTran comTran;
    private String CARD_CORP_CD, CARD_NO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employee);
        listEmployee = new ArrayList<>();
        listSearch = new ArrayList<>();
        listViewEmployee = (ListView) findViewById(R.id.lv_employee);
        etSearch = (EditText) findViewById(R.id.et_search);
        listViewSearch = (ListView) findViewById(R.id.lv_search);
        employeeAdapter = new EmployeeAdapter(this, listEmployee, "");
      searchAdapter = new EmployeeAdapter(this, listSearch, "");
        listViewSearch.setAdapter(searchAdapter);
        listViewEmployee.setAdapter(employeeAdapter);
//
        Bundle bundle = getIntent().getExtras();
        MyPageItem item=(MyPageItem)bundle.getParcelable("CARD_CD");
        CARD_CORP_CD =item.getCARD_CORP_CD();
        CARD_NO = item.getCARD_NO();



//        if (bundle != null) {
//            CARD_CORP_CD = bundle.getString(CARD_CORP_CD);
//            Log.d("CARD_CORP_CD1","data: "+CARD_CORP_CD );
//            CARD_NO = bundle.getString(CARD_NO);
//
//        }
//        CARD_CORP_CD = getIntent().getExtras().getString(CARD_CORP_CD);
        comTran = new ComTran(this, this);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    listViewEmployee.setVisibility(View.VISIBLE);
                    listViewSearch.setVisibility(View.GONE);
                    employeeAdapter.notifyDataSetChanged();
                } else {
                    listViewEmployee.setVisibility(View.GONE);
                    listViewSearch.setVisibility(View.VISIBLE);
                    searchAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    searchResult(s.toString());
                   etSearch.getText().toString().toLowerCase(Locale.getDefault());

                }
            }
        });
        loadEmployee();
    }

    private void searchResult(String search) {
        listSearch.clear();
        searchAdapter.setSearch(search);
        for (int i = 0; i < listEmployee.size(); i++) {
            if (SoundSearcher.matchString(listEmployee.get(i).getBP_EMPL_NM().toLowerCase(), search.toLowerCase())) {
                listSearch.add(listEmployee.get(i));
            } else if (SoundSearcher.matchString(listEmployee.get(i).getBP_DVSN_NM().toLowerCase(), search.toLowerCase())) {
                listSearch.add(listEmployee.get(i));
            } else if (SoundSearcher.matchString(listEmployee.get(i).getCLPH_NO(), search.replace("-", ""))) {
                listSearch.add(listEmployee.get(i));
            }
        }
        DevLog.devLog("List Search", listSearch.toString());
        Log.d("Emp Search Final", listSearch.size() + "");


        searchAdapter.notifyDataSetChanged();

    }

    public void loadEmployee() {
        try {
            TX_MYCD_MBL_L007_REQ req = new TX_MYCD_MBL_L007_REQ();
            req.setCARD_CORP_CD(CARD_CORP_CD);
            Log.d("CARD_CORP_CD2","data: "+CARD_CORP_CD );
            req.setCARD_NO(CARD_NO);
//

            comTran.requestData(TX_MYCD_MBL_L007_REQ.TXNO, req.getSendMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try {
            if (tranCode.equals(TX_MYCD_MBL_L007_REQ.TXNO)) {
                TX_MYCD_MBL_L007_RES res = new TX_MYCD_MBL_L007_RES(object);
                TX_MYCD_MBL_L007_RES_REC rec = res.getBP_EMPL_REC();
                for (int i = 0; i < rec.getLength(); i++) {
                    Employee emp = new Employee();
                    emp.setID(listEmployee.size());
                    emp.setBP_EMPL_NO(rec.getBP_EMPL_NO());
                    emp.setBP_EMPL_NM(rec.getBP_EMPL_NM());
                    emp.setCLPH_NO(rec.getCLPH_NO());
                    emp.setBP_DVSN_NM(rec.getBP_DVSN_NM());
                    listEmployee.add(emp);
                    rec.moveNext();
                }
                employeeAdapter.notifyDataSetChanged();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
