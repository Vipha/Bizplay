package com.example.kosign.introslider.carouselview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kosign.introslider.AddEmployeeActivity;
import com.example.kosign.introslider.Menu_card;
import com.example.kosign.introslider.R;
import com.example.kosign.introslider.directionalcarousel.page.PageFragment;
import com.example.kosign.introslider.directionalcarousel.page.PageLayout;

public class MyPageFragment extends PageFragment<MyPageItem> {

    Spannable.Factory spannableFactory;

    @Override
    public View setupPage(PageLayout pageLayout, MyPageItem pageItem) {
        View pageContent = pageLayout.findViewById(R.id.page_content);
        TextView card_org = (TextView) pageContent.findViewById(R.id.tv_card_org);

        LinearLayout ll_card_start, ll_card_end, ll_month, ll_year;

        ll_card_start = (LinearLayout) pageLayout.findViewById(R.id.ll_card_start);
        ll_card_end = (LinearLayout) pageLayout.findViewById(R.id.ll_card_end);
        ll_month = (LinearLayout) pageLayout.findViewById(R.id.ll_month);
        ll_year = (LinearLayout) pageLayout.findViewById(R.id.ll_year);

        String card_num = pageItem.getCARD_NO();
        spannableFactory = Spannable.Factory.getInstance();



        /*TextView card_number = (TextView) pageContent.findViewById(R.id.tv_card_number);
        if(pageItem.getCARD_CORP_CD().equals("30000004") || pageItem.getCARD_CORP_CD().equals("30000006") || pageItem.getCARD_CORP_CD().equals("30000010")
           || pageItem.getCARD_CORP_CD().equals("30000012") || pageItem.getCARD_CORP_CD().equals("30000013") || pageItem.getCARD_CORP_CD().equals("30000014")
           || pageItem.getCARD_CORP_CD().equals("30000017") || pageItem.getCARD_CORP_CD().equals("30000020")) {
            card_number.setText(getIconTextColor(getContext(), card_num, 6));
        }else if(pageItem.getCARD_CORP_CD().equals("30000002") || pageItem.getCARD_CORP_CD().equals("30000003") || pageItem.getCARD_CORP_CD().equals("30000008") || pageItem.getCARD_CORP_CD().equals("30000015")
                || pageItem.getCARD_CORP_CD().equals("30000016") || pageItem.getCARD_CORP_CD().equals("30000018") || pageItem.getCARD_CORP_CD().equals("30000019") || pageItem.getCARD_CORP_CD().equals("30000021")
                || pageItem.getCARD_CORP_CD().equals("30000060") || pageItem.getCARD_CORP_CD().equals("30000061") || pageItem.getCARD_CORP_CD().equals("30000062") || pageItem.getCARD_CORP_CD().equals("30000063")
                || pageItem.getCARD_CORP_CD().equals("30000064")) {
            card_number.setText(getIconText(getContext(), card_num, 6));
        } else {
            card_number.setText(getIconTextBlack(getContext(), card_num, 6));
        }*/

       /* TextView tv_card_start = (TextView) pageContent.findViewById(R.id.tv_card_start);
        TextView tv_card_end = (TextView) pageContent.findViewById(R.id.tv_card_end);

        String card_s = card_num.substring(0, 4);
        String card_e = card_num.substring(12, card_num.length());


        tv_card_start.setText(getNumberIconText(getContext(), card_s));
        tv_card_end.setText(getNumberIconText(getContext(), card_e));
        TextView tv_month = (TextView) pageContent.findViewById(R.id.tv_month);
        TextView tv_day = (TextView) pageContent.findViewById(R.id.tv_day);
        if(!TextUtils.isEmpty(pageItem.getEXPR_TRM())) {
            String month = pageItem.getEXPR_TRM().substring(0,2);
            String year = pageItem.getEXPR_TRM().substring(3,pageItem.getEXPR_TRM().length());

            tv_month.setText(getNumberIconExp(getContext(), month));
            tv_day.setText(getNumberIconExp(getContext(), day));
        } else {
            tv_month.setText("");
            tv_day.setText("");
        }*/

        String card_s = card_num.substring(0, 4);
        String card_e = card_num.substring(12, card_num.length());

        setUpCardView(ll_card_start, card_s, 11,15);
        setUpCardView(ll_card_end, card_e, 11, 15);

        if(!TextUtils.isEmpty(pageItem.getEXPR_TRM())) {
            String month = pageItem.getEXPR_TRM().substring(0,2);
            String year = pageItem.getEXPR_TRM().substring(3,pageItem.getEXPR_TRM().length());

            setUpCardView(ll_month, month, 9, 12);
            setUpCardView(ll_year, year, 9, 12);
        }




        TextView card_biz = (TextView) pageContent.findViewById(R.id.tv_card_biz);
        if(pageItem.getBIZ_NM().equalsIgnoreCase("null") || TextUtils.isEmpty(pageItem.getBIZ_NM()) == true) {
            card_biz.setText("");
        }else
            card_biz.setText(pageItem.getBIZ_NM());

        if(pageItem.getCARD_CORP_CD().equals("30000001")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_kb);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_kb, R.drawable.card_kb_d);

        } else if(pageItem.getCARD_CORP_CD().equals("30000002")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_hd);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_hd, R.drawable.card_hd_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000003")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_ss);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_ss, R.drawable.card_ss_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000008")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_sh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_sh, R.drawable.card_sh_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000010")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_hn);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_hn, R.drawable.card_hn_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000015")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_hn);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_hn, R.drawable.card_hn_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000016")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_ct);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_ct, R.drawable.card_ct_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000017")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_lt);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_lt, R.drawable.card_lt_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000018")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_wr);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_wr, R.drawable.card_wr_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000019")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_lt);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_lt, R.drawable.card_lt_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000021")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_nh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_nh, R.drawable.card_nh_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000060")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_ibk);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_ibk, R.drawable.card_ibk_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000061")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_sc);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_sc, R.drawable.card_sc_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000062")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_bs);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_bs, R.drawable.card_bs_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000063")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_tk);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_tk, R.drawable.card_tk_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000064")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_kn);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_kn, R.drawable.card_kn_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000065")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_wr);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_wr, R.drawable.card_wr_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000066")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_hn);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_hn, R.drawable.card_hn_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000067")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_nh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_nh, R.drawable.card_nh_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000068")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_kb);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_kb, R.drawable.card_kb_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000069")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_sh);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_sh, R.drawable.card_sh_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000070")) {
            card_biz.setTextColor(Color.parseColor("#FFFFFF"));
            //pageContent.setBackgroundResource(R.drawable.card_ct);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_ct, R.drawable.card_ct_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000004")) {  //From here N/A

            if(pageItem.getCARD_ORG_NM().equalsIgnoreCase("null") || TextUtils.isEmpty(pageItem.getCARD_ORG_NM()) == true) {
                card_org.setText("");
            } else
                card_org.setText(pageItem.getCARD_ORG_NM());

            card_biz.setTextColor(Color.parseColor("#333333"));
            //pageContent.setBackgroundResource(R.drawable.card_white);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000006")) {

            if(pageItem.getCARD_ORG_NM().equalsIgnoreCase("null") || TextUtils.isEmpty(pageItem.getCARD_ORG_NM()) == true) {
                card_org.setText("");
            } else
                card_org.setText(pageItem.getCARD_ORG_NM());

            card_biz.setTextColor(Color.parseColor("#333333"));
            //pageContent.setBackgroundResource(R.drawable.card_white);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000012")) {

            if(pageItem.getCARD_ORG_NM().equalsIgnoreCase("null") || TextUtils.isEmpty(pageItem.getCARD_ORG_NM()) == true) {
                card_org.setText("");
            } else
                card_org.setText(pageItem.getCARD_ORG_NM());

            card_biz.setTextColor(Color.parseColor("#333333"));
            //pageContent.setBackgroundResource(R.drawable.card_white);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000013")) {

            if(pageItem.getCARD_ORG_NM().equalsIgnoreCase("null") || TextUtils.isEmpty(pageItem.getCARD_ORG_NM()) == true) {
                card_org.setText("");
            } else
                card_org.setText(pageItem.getCARD_ORG_NM());

            card_biz.setTextColor(Color.parseColor("#333333"));
            //pageContent.setBackgroundResource(R.drawable.card_white);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000014")) {

            if(pageItem.getCARD_ORG_NM().equalsIgnoreCase("null") || TextUtils.isEmpty(pageItem.getCARD_ORG_NM()) == true) {
                card_org.setText("");
            } else
                card_org.setText(pageItem.getCARD_ORG_NM());

            card_biz.setTextColor(Color.parseColor("#333333"));
            //pageContent.setBackgroundResource(R.drawable.card_white);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        } else if(pageItem.getCARD_CORP_CD().equals("30000020")) {

            if(pageItem.getCARD_ORG_NM().equalsIgnoreCase("null") || TextUtils.isEmpty(pageItem.getCARD_ORG_NM()) == true) {
                card_org.setText("");
            } else
                card_org.setText(pageItem.getCARD_ORG_NM());

            card_biz.setTextColor(Color.parseColor("#333333"));
            //pageContent.setBackgroundResource(R.drawable.card_white);
            setCardBackground(pageItem.getEXPR_TRM(), pageContent, R.drawable.card_white, R.drawable.card_white_d);
        }


        return pageContent;
    }

    public void setCardBackground(String date, View view, int resId1, int resId2){
        if(!TextUtils.isEmpty(date)){
            view.setBackgroundResource(resId1);
        }else view.setBackgroundResource(resId2);
    }


   /* public Spannable getNumberIconExp(Context context, CharSequence text) {
        StringBuilder stringBuilder = new StringBuilder(text);
        Spannable spannable = spannableFactory.newSpannable(stringBuilder);
        int index = text.length()-1;
        String[] all = {String.valueOf(text.charAt(0)),String.valueOf(text.charAt(1))};
        for(int i = 0; i<=index; i++){
            if(all[i].equals("0"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_0),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("1"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_1),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("2"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_2),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("3"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_3),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("4"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_4),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("5"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_5),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("6"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_6),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("7"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_7),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("8"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_8),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i].equals("9"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_9),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
        return spannable;
    }*/

    // add image number to card layout
    public void setUpCardView(LinearLayout linearLayout, String text, int width, int height){
        for(int i=0;i<text.length();i++){
            linearLayout.addView(getViewItem(text.substring(i,i+1), width, height));
        }
    }

    // create view number item
    public View getViewItem(String str, int width, int height){
        int index = Integer.parseInt(str);
        int[]res = {R.drawable.cnum_0, R.drawable.cnum_1, R.drawable.cnum_2, R.drawable.cnum_3, R.drawable.cnum_4, R.drawable.cnum_5,
                    R.drawable.cnum_6, R.drawable.cnum_7, R.drawable.cnum_8, R.drawable.cnum_9};
        return getImageView(res[index], width, height);
    }

    // create image view programmatically
    public ImageView getImageView(int resId, int width, int height){
        ImageView img = new ImageView(getActivity());
        img.setLayoutParams(new LinearLayout.LayoutParams(toDP(width), toDP(height)));
        img.setImageDrawable(getActivity().getResources().getDrawable(resId));
        return img;
    }

    private int toDP(float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    /*public Spannable getNumberIconText(Context context, CharSequence text) {
        StringBuilder stringBuilder = new StringBuilder(text);
        Spannable spannable = spannableFactory.newSpannable(stringBuilder);
        int index = text.length()-1;
        //String[] all = {String.valueOf(text.charAt(0)),String.valueOf(text.charAt(1)),String.valueOf(text.charAt(2)),String.valueOf(text.charAt(3))};
        char[] all = text.toString().toCharArray();
        for(int i = 0; i<=index; i++){
            if(all[i]=='0')//.equals("0"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_0),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='1')//.equals("1"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_1),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='2')//.equals("2"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_2),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='3')//.equals("3"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_3),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='4')//.equals("4"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_4),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='5')//.equals("5"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_5),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='6')//.equals("6"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_6),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='7')//.equals("7"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_7),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='8')//.equals("8"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_8),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if(all[i]=='9')//.equals("9"))
                spannable.setSpan(new ImageSpan(context, R.drawable.cnum_9),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
        return spannable;
    }*/


    public Spannable getIconText(Context context, CharSequence text, int start_length) {
        StringBuilder stringBuilder = new StringBuilder(text);
        Spannable spannable = spannableFactory.newSpannable(stringBuilder);
        int index = text.length() - 1;
        for(int i = start_length; i<=index; i++){
            if(i<start_length+11){
                if(i==10 || i==11 || i==12) continue;
                spannable.setSpan(new ImageSpan(context, R.drawable.card_img),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return spannable;
    }

    public Spannable getIconTextBlack(Context context, CharSequence text, int start_length) {
        StringBuilder stringBuilder = new StringBuilder(text);
        Spannable spannable = spannableFactory.newSpannable(stringBuilder);
        int index = text.length() - 1;
        for(int i = start_length; i<=index; i++){
            if(i<start_length+11){
                if(i==10 || i==11 || i==12) continue;
                spannable.setSpan(new ImageSpan(context, R.drawable.card_img_black),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return spannable;
    }

    public Spannable getIconTextColor(Context context, CharSequence text, int start_length) {
        StringBuilder stringBuilder = new StringBuilder(text);
        Spannable spannable = spannableFactory.newSpannable(stringBuilder);
        int index = text.length() - 1;
        for(int i = start_length; i<=index; i++){
            if(i<start_length+11){
                if(i==10 || i==11 || i==12) continue;
                spannable.setSpan(new ImageSpan(context, R.drawable.card_img_color),i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return spannable;
    }
}
