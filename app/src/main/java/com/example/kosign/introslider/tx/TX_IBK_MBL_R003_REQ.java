package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;
import org.json.JSONException;
/*
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_IBK_MBL_R003_REQ extends TxMessage {

    public static final String TXNO =   "IBK_MBL_R003";

    private TX_IBK_MBL_R003_REQ_DATA   mTxKeyData;

    public TX_IBK_MBL_R003_REQ() throws Exception{
        mTxKeyData  = new TX_IBK_MBL_R003_REQ_DATA();
        super.initSendMessage();
    }


    public void setRCPT_TX_STS(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.RCPT_TX_STS,value);
    }

    public void setCDN(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.CDN,value);
    }

    public void setATHZ_YMD(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.ATHZ_YMD,value);
    }

    public void setATHZ_HMS(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.ATHZ_HMS,value);
    }

    public void setAPN(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.APN,value);
    }

    public void setAFST_BZN(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.AFST_BZN,value);
    }

    public void setAFST_NO(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.AFST_NO,value);
    }

    public void setATHZ_AMT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.ATHZ_AMT,value);
    }

    public void setCNCL_YMD(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CNCL_YMD,value);
    }

    public void setCNCL_AMT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CNCL_AMT,value);
    }

    public void setRCPT_SEQNO(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.RCPT_SEQNO,value);
    }

    private class TX_IBK_MBL_R003_REQ_DATA {
        private String RCPT_TX_STS      =       "RCPT_TX_STS"; //영수증등록 거래상태
        private String CDN              =       "CDN";
        private String ATHZ_YMD         =       "ATHZ_YMD";
        private String ATHZ_HMS         =       "ATHZ_HMS";
        private String APN              =       "APN";
        private String AFST_BZN         =       "AFST_BZN";
        private String AFST_NO          =       "AFST_NO";
        private String ATHZ_AMT         =       "ATHZ_AMT";
        private String CNCL_YMD         =       "CNCL_YMD";
        private String CNCL_AMT         =       "CNCL_AMT";
        private String RCPT_SEQNO       =       "RCPT_SEQNO";
    }

}
