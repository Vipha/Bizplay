package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/*
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_IBK_MBL_R003_RES extends TxMessage {


    private TX_IBK_MBL_R003_RES_DATA mTxKeyData;

    public TX_IBK_MBL_R003_RES(Object obj)throws Exception{
        mTxKeyData  = new TX_IBK_MBL_R003_RES_DATA();
        super.initRecvMessage(obj);
    }


    public String getCD_RCPT_STS(String q) throws JSONException {
        return getString(mTxKeyData.CD_RCPT_STS);
    }

    public String getTAX_TYPE_CD(String s) throws JSONException{
        return getString(mTxKeyData.TAX_TYPE_CD);
    }

    public String getVAT_RFND_STS(String s) throws JSONException{
        return getString(mTxKeyData.VAT_RFND_STS);
    }

    public String getCARD_CORP_CD() throws JSONException{
        return getString(mTxKeyData.CARD_CORP_CD);
    }

    public String getAPN_AMT(String s) throws JSONException{
        return getString(mTxKeyData.APN_AMT);
    }

    public String getSUPL_AMT() throws JSONException{
        return getString(mTxKeyData.SUPL_AMT);
    }

    public String getVAT_AMT() throws JSONException{
        return getString(mTxKeyData.VAT_AMT);
    }

    public String getTRAN_KIND_CD(String q) throws JSONException{
        return getString(mTxKeyData.TRAN_KIND_CD);
    }

    public String getTRAN_KIND_NM() throws JSONException{
        return getString(mTxKeyData.TRAN_KIND_NM);
    }

    public String getSUMMARY() throws JSONException{
        return getString(mTxKeyData.SUMMARY);
    }

    public String getRPPR_NM() throws JSONException{
        return getString(mTxKeyData.RPPR_NM);
    }

    public String getAFST_NM() throws JSONException{
        return getString(mTxKeyData.AFST_NM);
    }

    public String getAFST_TPN() throws JSONException{
        return getString(mTxKeyData.AFST_TPN);
    }

    public String getAFST_ZPCD() throws JSONException{
        return getString(mTxKeyData.AFST_ZPCD);
    }

    public String getAFST_ADR() throws JSONException{
        return getString(mTxKeyData.AFST_ADR);
    }

    public String getAFST_TPBCD() throws JSONException{
        return getString(mTxKeyData.AFST_TPBCD);
    }

    public String getAFST_TPBCD_NM() throws JSONException{
        return getString(mTxKeyData.AFST_TPBCD_NM);
    }

    public String getRCPT_RFS_CNT() throws JSONException{
        return getString(mTxKeyData.RCPT_RFS_CNT);
    }

    public TX_IBK_MBL_R003_RES_RCPT_RFS_REC getRCPT_RFS_REC() throws Exception {
        return new TX_IBK_MBL_R003_RES_RCPT_RFS_REC(getRecord(mTxKeyData.RCPT_RFS_REC));
    }

    public String getRCPT_USER_CNT() throws JSONException{
        return getString(mTxKeyData.CD_RCPT_STS);
    }

    public TX_IBK_MBL_R003_RES_RCPT_USER_REC getRCPT_USER_REC() throws Exception{
        return new TX_IBK_MBL_R003_RES_RCPT_USER_REC(getRecord(mTxKeyData.RCPT_USER_REC));
    }

    public TX_IBK_MBL_R003_RES_RCPT_IMG_REC getRCPT_IMG_REC() throws Exception {
        return new TX_IBK_MBL_R003_RES_RCPT_IMG_REC(getRecord(mTxKeyData.RCPT_IMG_REC));
    }

    public String getPTL_ID() throws JSONException{
        return getString(mTxKeyData.PTL_ID);
    }

    public String getUSE_INTT_ID() throws JSONException{
        return getString(mTxKeyData.USE_INTT_ID);
    }

    public String getCHNL_ID() throws JSONException{
        return getString(mTxKeyData.CHNL_ID);
    }

    public String getUSER_ID() throws JSONException{
        return getString(mTxKeyData.USER_ID);
    }



    private class TX_IBK_MBL_R003_RES_DATA {

        private String  CD_RCPT_STS     =       "CD_RCPT_STS";
        private String  TAX_TYPE_CD     =       "TAX_TYPE_CD";
        private String  VAT_RFND_STS    =       "VAT_RFND_STS";
        private String  CARD_CORP_CD    =       "CARD_CORP_CD";
        private String  APN_AMT         =       "APN_AMT";
        private String  SUPL_AMT        =       "SUPL_AMT";
        private String  VAT_AMT         =       "VAT_AMT";
        private String  TRAN_KIND_CD    =       "TRAN_KIND_CD";
        private String  TRAN_KIND_NM    =       "TRAN_KIND_NM";
        private String  SUMMARY         =       "SUMMARY";
        private String  RPPR_NM         =       "RPPR_NM";
        private String  AFST_NM         =       "AFST_NM";
        private String  AFST_TPN        =       "AFST_TPN";
        private String  AFST_ZPCD       =       "AFST_ZPCD";
        private String  AFST_ADR        =       "AFST_ADR";
        private String  AFST_TPBCD      =       "AFST_TPBCD";
        private String  AFST_TPBCD_NM   =       "AFST_TPBCD_NM";
        private String  RCPT_RFS_CNT    =       "RCPT_RFS_CNT";
        private String  RCPT_RFS_REC    =       "RCPT_RFS_REC";
        private String  RCPT_USER_CNT   =       "RCPT_USER_CNT";
        private String  RCPT_USER_REC   =       "RCPT_USER_REC";
        private String  RCPT_IMG_REC    =       "RCPT_IMG_REC";
        private String  PTL_ID          =       "PTL_ID";
        private String  USE_INTT_ID     =       "USE_INTT_ID";
        private String  CHNL_ID         =       "CHNL_ID";
        private String  USER_ID         =       "USER_ID";
    }
}
