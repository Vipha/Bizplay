package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * receipt list response
 * Created by Bunna on 0001, 01/07/2016.
 */
public class TX_MYCD_MBL_L002_RES extends TxMessage {

    private  TX_MYCD_MBL_L001_RES_DATA mTxKeyData; // 전문명세 필드

    public TX_MYCD_MBL_L002_RES(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L001_RES_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 승인일자NEXTKEY
     * @throws Exception
     */
    public String getINQ_NEXT_APV_DT() throws JSONException,Exception {
        return getString(mTxKeyData.INQ_NEXT_APV_DT);
    }

    /**
     * 일련번호NEXTKEY
     * @throws Exception
     */
    public String getINQ_NEXT_APV_SEQ() throws JSONException,Exception {
        return getString(mTxKeyData.INQ_NEXT_APV_SEQ);
    }

    /**
     * 승인시각NEXTKEY
     * @throws Exception
     */
    public String getINQ_NEXT_APV_TM() throws JSONException,Exception {
        return getString(mTxKeyData.INQ_NEXT_APV_TM);
    }

    /**
     * 승인번호NEXTKEY
     * @throws Exception
     */
    public String getINQ_NEXT_APV_NO() throws JSONException,Exception {
        return getString(mTxKeyData.INQ_NEXT_APV_NO);
    }

    /**
     * 반복건수
     * @throws Exception
     */
    public String getINQ_ITRN_SUB_ROWCOUNT() throws JSONException,Exception {
        return getString(mTxKeyData.INQ_ITRN_SUB_ROWCOUNT);
    }



    /**
     * 반복부
     * @throws Exception
     */
    public TX_MYCD_MBL_L002_RES_REC getRECEIPT_LIST() throws JSONException,Exception {
        return new TX_MYCD_MBL_L002_RES_REC(getRecord(mTxKeyData.INQ_ITRN_SUB));
    }

    /**
     * 거래내역 뱃지표시 건수
     * @throws Exception
     */
    public String getTRAN_CNT() throws JSONException,Exception {
        return getString(mTxKeyData.TRAN_CNT);
    }

    /**
     * 알림내역 뱃지표시 건수 (미처리영수증건수)
     * @throws Exception
     */
    public String getNOTI_CNT() throws JSONException,Exception {
        return getString(mTxKeyData.NOTI_CNT);
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L001_RES_DATA {
        private String INQ_NEXT_APV_DT		= 	"INQ_NEXT_APV_DT";				//승인일자NEXTKEY
        private String INQ_NEXT_APV_SEQ		= 	"INQ_NEXT_APV_SEQ";				//일련번호NEXTKEY
        private String INQ_NEXT_APV_TM		= 	"INQ_NEXT_APV_TM";				//승인시각NEXTKEY
        private String INQ_NEXT_APV_NO		= 	"INQ_NEXT_APV_NO";				//승인번호NEXTKEY
        private String INQ_ITRN_SUB_ROWCOUNT		= 	"INQ_ITRN_SUB_ROWCOUNT";				//반복건수
        private String INQ_ITRN_SUB		        = 	"INQ_ITRN_SUB";				        //반복부
        private String TRAN_CNT		= 	"TRAN_CNT";				//거래내역 뱃지표시 건수
        private String NOTI_CNT		= 	"NOTI_CNT";				//알림내역 뱃지표시 건수 (미처리영수증건수)
    }

}
