package com.example.kosign.introslider;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.example.kosign.introslider.tran.ComTran;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L002_REQ;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L002_RES;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L002_RES_REC;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_R001_REQ;
import com.webcash.sws.log.DevLog;

import org.json.JSONException;

import java.util.List;


public class Menu_Bar extends Activity implements  ComTran.OnComTranListener {
    private List<Receipt> all_receiptList;
    private static final String INQ_CD = "0";
    public static String INQ_NEXT_APV_DT = "";
    public static String INQ_NEXT_APV_SEQ = "";
    public static String INQ_NEXT_APV_TM = "";
    public static String INQ_NEXT_APV_NO = "";
    public static String selected_card_number = "", selected_card_name = "", selected_card_corp_cd = "";
    private ComTran mComTran;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a006_actionbar);
        mComTran=new ComTran(this,this);
        callR001();
        loadReceiptList();

    }

    private void loadReceiptList() {
        try {
            TX_MYCD_MBL_L002_REQ req = new TX_MYCD_MBL_L002_REQ();
            req.setINQ_CD(INQ_CD);
            req.setCARD_CORP_CD(selected_card_corp_cd);
            req.setCARD_NO(selected_card_number);
            req.setINQ_NEXT_APV_DT(INQ_NEXT_APV_DT);
            req.setINQ_NEXT_APV_NO(INQ_NEXT_APV_NO);
            req.setINQ_NEXT_APV_SEQ(INQ_NEXT_APV_SEQ);
            req.setINQ_NEXT_APV_TM(INQ_NEXT_APV_TM);


            mComTran.requestData(TX_MYCD_MBL_L002_REQ.TXNO, req.getSendMessage());
            Log.i("connect","Connect");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
        @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
            TX_MYCD_MBL_L002_RES res = new TX_MYCD_MBL_L002_RES(object);

            //for pagination receipt
            INQ_NEXT_APV_DT = res.getINQ_NEXT_APV_DT();
            INQ_NEXT_APV_SEQ = res.getINQ_NEXT_APV_SEQ();
            INQ_NEXT_APV_NO = res.getINQ_NEXT_APV_NO();
            INQ_NEXT_APV_TM = res.getINQ_NEXT_APV_TM();

            TX_MYCD_MBL_L002_RES_REC rec = res.getRECEIPT_LIST();

            Receipt receipt = new Receipt();
            receipt.setCARD_NO(rec.getCARD_NO());
            receipt.setCARD_NO(rec.getCARD_NO());
            receipt.setAPV_DT(rec.getAPV_DT());
            receipt.setAPV_SEQ(rec.getAPV_SEQ());
            receipt.setAPV_TM(rec.getAPV_TM());
            receipt.setAPV_NO(rec.getAPV_NO());
            receipt.setAPV_AMT(rec.getAPV_AMT());
            receipt.setAPV_CAN_YN(rec.getAPV_CAN_YN());
            receipt.setAPV_CAN_DT(rec.getAPV_CAN_DT());
            DevLog.devLog("APV_CAN_DT", rec.getAPV_CAN_DT());
            receipt.setITLM_MMS_CNT(rec.getITLM_MMS_CNT());
            receipt.setSETL_SCHE_DT(rec.getSETL_SCHE_DT());
            receipt.setMEST_NM(rec.getMEST_NM());
            receipt.setMEST_NO(rec.getMEST_NO());
            receipt.setMEST_BIZ_NO(rec.getMEST_BIZ_NO());
            receipt.setRCPT_TX_STS(rec.getRCPT_TX_STS());
            receipt.setRCPT_RFS_STS(rec.getRCPT_RFS_STS());
            receipt.setCARD_NICK_NM(rec.getCARD_NICK_NM());
            receipt.setDAY_NM(rec.getDAY_NM());
            receipt.setTRAN_KIND_CD(rec.getTRAN_KIND_CD());
            receipt.setTRAN_KIND_NM(rec.getTRAN_KIND_NM());
            all_receiptList.add(receipt);
        }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
    private void callR001() {
        try{
            TX_MYCD_MBL_R001_REQ req = new TX_MYCD_MBL_R001_REQ();
            req.setCARD_NO(selected_card_number);
            req.setCARD_CORP_CD(selected_card_corp_cd);
            req.setINQ_GB("0");

            mComTran.requestData(TX_MYCD_MBL_R001_REQ.TXNO, req.getSendMessage(), true );
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
