package com.example.kosign.introslider.tx;



import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by KOSIGN on 10/6/2017.
 *
 * MYCD_MBL_P004 Y Registered receipt manager information by corporate card
 */

public class TX_MYCD_MBL_P004_REQ extends TxMessage{

    public static final String TXNO =   "MYCD_MBL_P004";

    private TX_MYCD_MBL_P004_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_P004_REQ()throws Exception{
        mTxKeyData= new TX_MYCD_MBL_P004_REQ_DATA();
        super.initSendMessage();
    }



    public void setCARD_CORP_CD(String CARD_CORP_CD) throws JSONException {
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, CARD_CORP_CD);
    }

    public void setCARD_NO(String CARD_NO)  throws JSONException{
        mSendMessage.put(mTxKeyData.CARD_NO, CARD_NO);
    }

    public void setRCPT_USER_REC(JSONArray RCPT_USER_REC) throws JSONException {
        mSendMessage.put(mTxKeyData.RCPT_USER_REC, RCPT_USER_REC);
    }

    public void setAPV_GB(String APV_GB)  throws JSONException{
       mSendMessage.put(mTxKeyData.APV_GB,APV_GB);
    }

    public void setAPV_DT(String APV_DT)  throws JSONException{
        mSendMessage.put(mTxKeyData.APV_DT,APV_DT);
    }

    public void setAPV_SEQ(String APV_SEQ)  throws JSONException{
        mSendMessage.put(mTxKeyData.APV_SEQ,APV_SEQ);
    }

    public void setAPV_TM(String APV_TM)  throws JSONException{
        mSendMessage.put(mTxKeyData.APV_TM,APV_TM);
    }

    public void setAPV_NO(String APV_NO)  throws JSONException{
        mSendMessage.put(mTxKeyData.APV_NO,APV_NO);
    }



    private class TX_MYCD_MBL_P004_REQ_DATA {

        private String CARD_CORP_CD =   "CARD_CORP_CD";
        private String CARD_NO  =   "CARD_NO";
        private String RCPT_USER_REC    =   "RCPT_USER_REC";
        private String APV_GB   =   "APV_GB";
        private String APV_DT   =   "APV_DT";
        private String APV_SEQ  =   "APV_SEQ";
        private String APV_TM   =   "APV_TM";
        private String APV_NO   =   "APV_NO";
    }
}
