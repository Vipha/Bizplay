package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_MYCD_MBL_P007_REQ extends TxMessage {

    public static final String TXNO =  "MYCD_MBL_P007"; //User Login (Password Encryption) req data


    private TX_MYCD_MBL_P007_REQ_DATA mTxKeyData;


     public TX_MYCD_MBL_P007_REQ() throws Exception{
         mTxKeyData=new TX_MYCD_MBL_P007_REQ_DATA();
         super.initSendMessage();
     }


    public void setUSER_ID(String USER_ID) throws JSONException {
        mSendMessage.put(mTxKeyData.USER_ID, USER_ID);
    }

    public void setENC_GB(String ENC_GB) throws JSONException {
        mSendMessage.put(mTxKeyData.ENC_GB,ENC_GB);
    }

    public void setUSER_PW(String USER_PW) throws JSONException{
        mSendMessage.put(mTxKeyData.USER_PW,USER_PW);
    }

    public void setMOBL_CD(String MOBL_CD) throws JSONException {
        mSendMessage.put(mTxKeyData.MOBL_CD,MOBL_CD);
    }

    public void setSESSION_ID(String SESSION_ID) throws JSONException {
        mSendMessage.put(mTxKeyData.SESSION_ID,SESSION_ID);
    }

    public void setTOKEN(String TOKEN) throws JSONException {
        mSendMessage.put(mTxKeyData.TOKEN,TOKEN);
    }


    private class TX_MYCD_MBL_P007_REQ_DATA {
        private String USER_ID  =   "USER_ID";
        private String ENC_GB   =   "ENC_GB";
        private String USER_PW  =   "USER_PW";
        private String MOBL_CD  =   "MOBL_CD";
        private String SESSION_ID   =   "SESSION_ID";



        private String TOKEN    =   "TOKEN";
    }
}
