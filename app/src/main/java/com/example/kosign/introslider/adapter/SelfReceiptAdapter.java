package com.example.kosign.introslider.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kosign.introslider.R;
import com.example.kosign.introslider.Receipt;


import java.text.DecimalFormat;
import java.util.List;

/*
 * Created by bunna on 6/3/2016.
 */
public class SelfReceiptAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<Receipt> receiptList;

    public SelfReceiptAdapter(Activity activity, List<Receipt> movieList) {
        this.activity = activity;
        this.receiptList = movieList;
    }

    @Override
    public int getCount() {
        return receiptList.size();
    }

    @Override
    public Object getItem(int position) {
        return receiptList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        try{

            if (inflater == null)
                inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)
                convertView = inflater.inflate(R.layout.receipt_list_item, null);

            RelativeLayout rl_receipt = (RelativeLayout) convertView.findViewById(R.id.rl_receipt);
            TextView tv_receipt_place = (TextView) convertView.findViewById(R.id.tv_receipt_place);
            TextView tv_receipt_date = (TextView) convertView.findViewById(R.id.tv_receip_date);
            TextView tv_receipt_holder = (TextView) convertView.findViewById(R.id.tv_receipt_holder);
            TextView tv_receipt_amount = (TextView) convertView.findViewById(R.id.tv_receipt_amount);
            TextView tv_receipt_won = (TextView) convertView.findViewById(R.id.tv_receipt_one);
            ImageView iv_receipt_self = (ImageView) convertView.findViewById(R.id.iv_receipt_self);
            TextView tv_receipt_return = (TextView) convertView.findViewById(R.id.tv_receipt_return);
            TextView tv_minus = (TextView) convertView.findViewById(R.id.tv_minus);
            TextView tv_receipt_completed = (TextView) convertView.findViewById(R.id.tv_receipt_completed_name);

            Receipt receipt = receiptList.get(position);

            tv_receipt_completed.setVisibility(View.VISIBLE);
            tv_receipt_completed.setTextColor(Color.parseColor("#a4a4a4"));

            tv_receipt_place.setTextColor(Color.parseColor("#a4a4a4"));
            tv_receipt_date.setTextColor(Color.parseColor("#a4a4a4"));
            tv_receipt_holder.setTextColor(Color.parseColor("#a4a4a4"));
            tv_receipt_amount.setTextColor(Color.parseColor("#a4a4a4"));
            tv_receipt_won.setTextColor(Color.parseColor("#a4a4a4"));
            rl_receipt.setBackgroundColor(Color.parseColor("#FFFFFF"));
            iv_receipt_self.setVisibility(View.VISIBLE);


//        tv_receipt_place.setText(receipt.getReceipt_place());
//        tv_receipt_date.setText(receipt.getReceipt_date());
//        tv_receipt_holder.setText(receipt.getReceipt_holder());
//        tv_receipt_amount.setText(receipt.getReceipt_amount());
//        tv_receipt_place.setText(receipt.getReceipt_place());
            DecimalFormat df = new DecimalFormat("###,###");

            if(receipt.getAPV_DT().length() == 8) {
                tv_receipt_date.setText(receipt.getAPV_DT().subSequence(4,6) + "월 " + receipt.getAPV_DT().substring(6, 8) + "일" + "("+ receipt.getDAY_NM()+")");
            } else {
                tv_receipt_date.setText(receipt.getAPV_DT() + "("+ receipt.getDAY_NM()+")");
            }
            if(receipt.getTRAN_KIND_NM().equalsIgnoreCase("null") || TextUtils.isEmpty(receipt.getTRAN_KIND_NM())) {
                tv_receipt_completed.setText("");
            } else {
                tv_receipt_completed.setText(receipt.getTRAN_KIND_NM());
            }
            tv_receipt_place.setText(receipt.getMEST_NM());
            tv_receipt_holder.setText(receipt.getCARD_ORG_NM() + receipt.getCARD_NO().substring(receipt.getCARD_NO().length() - 4));
            tv_receipt_amount.setText(df.format((long) Double.parseDouble(receipt.getAPV_AMT())));


            if(receipt.getAPV_CAN_YN().equalsIgnoreCase("b") == true) {
                tv_receipt_return.setVisibility(View.VISIBLE);

                tv_receipt_amount.setTextColor(Color.parseColor("#ff3f34"));
                tv_receipt_won.setTextColor(Color.parseColor("#ff3f34"));

                tv_minus.setVisibility(View.VISIBLE);

                tv_receipt_completed.setVisibility(View.INVISIBLE);

//                tv_receipt_amount.setPaintFlags(tv_receipt_amount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                tv_receipt_won.setPaintFlags(tv_receipt_won.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                tv_receipt_return.setVisibility(View.INVISIBLE);
                tv_minus.setVisibility(View.GONE);
            }

        }catch (Exception e) {
//            e.printStackTrace();
        }


        return convertView;
    }

}
