package com.example.kosign.introslider.carouselview;

import android.os.Parcel;
import android.os.Parcelable;

public class MyPageItem implements Parcelable {
    private String mTitle;
    /*
        CARD_CORP_CD
       30000001;"KB"
       30000002;"현대"
       30000003;"삼성"
       30000004;"외환"
       30000006;"비씨"
       30000008;"신한"
       30000010;"하나"
       30000012;"광주"
       30000013;"수협"
       30000014;"전북"
       30000015;"하나"
       30000016;"씨티"
       30000017;"아멕스"
       30000018;"우리"
       30000019;"롯데"
       30000020;"산은"
       30000021;"NH"
       30000060;"기업"
     */
    public String CARD_CORP_CD;
    public String CARD_NO;
    private String CARD_ORG_NM;
    private String BIZ_NM;
    private String CARD_NICK_NM;
    private String EXPR_TRM;

    public MyPageItem() {}

    public MyPageItem(String title) {
        mTitle = title;

    }

    private MyPageItem(Parcel in) {
        mTitle = in.readString();
        CARD_CORP_CD=in.readString();
        CARD_NO=in.readString();
    }

    public MyPageItem(String CARD_CORP_CD, String CARD_NO, String CARD_ORG_NM, String BIZ_NM, String CARD_NICK_NM, String EXPR_TRM) {
        this.CARD_CORP_CD = CARD_CORP_CD;
        this.CARD_NO = CARD_NO;
        this.CARD_ORG_NM = CARD_ORG_NM;
        this.BIZ_NM = BIZ_NM;
        this.CARD_NICK_NM = CARD_NICK_NM;
        this.EXPR_TRM = EXPR_TRM;
    }

    public void setTitle(String value) { this.mTitle = value;}
    public String getTitle() {
        return mTitle;
    }

    /*
        CARD_CORP_CD
       30000001;"KB"
       30000002;"현대"
       30000003;"삼성"
       30000004;"외환"
       30000006;"비씨"
       30000008;"신한"
       30000010;"하나"
       30000012;"광주"
       30000013;"수협"
       30000014;"전북"
       30000015;"하나"
       30000016;"씨티"
       30000017;"아멕스"
       30000018;"우리"
       30000019;"롯데"
       30000020;"산은"
       30000021;"NH"
       30000060;"기업"
     */
    public String getCARD_CORP_CD() {
        return CARD_CORP_CD;
    }

    /*
        CARD_CORP_CD
       30000001;"KB"
       30000002;"현대"
       30000003;"삼성"
       30000004;"외환"
       30000006;"비씨"
       30000008;"신한"
       30000010;"하나"
       30000012;"광주"
       30000013;"수협"
       30000014;"전북"
       30000015;"하나"
       30000016;"씨티"
       30000017;"아멕스"
       30000018;"우리"
       30000019;"롯데"
       30000020;"산은"
       30000021;"NH"
       30000060;"기업"
     */
    public void setCARD_CORP_CD(String CARD_CORP_CD) {
        this.CARD_CORP_CD = CARD_CORP_CD;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getCARD_ORG_NM() {
        return CARD_ORG_NM;
    }

    public void setCARD_ORG_NM(String CARD_ORG_NM) {
        this.CARD_ORG_NM = CARD_ORG_NM;
    }

    public String getBIZ_NM() {
        return BIZ_NM;
    }

    public void setBIZ_NM(String BIZ_NM) {
        this.BIZ_NM = BIZ_NM;
    }

    public String getCARD_NICK_NM() {
        return CARD_NICK_NM;
    }

    public void setCARD_NICK_NM(String CARD_NICK_NM) {
        this.CARD_NICK_NM = CARD_NICK_NM;
    }

    public String getEXPR_TRM() {
        return EXPR_TRM;
    }

    public void setEXPR_TRM(String EXPR_TRM) {
        this.EXPR_TRM = EXPR_TRM;
    }

    public static final Creator<MyPageItem> CREATOR =
            new Creator<MyPageItem>() {
                @Override
                public MyPageItem createFromParcel(Parcel in) {
                    return new MyPageItem(in);
                }

                @Override
                public MyPageItem[] newArray(int size) {
                    return new MyPageItem[size];
                }
            };

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mTitle);
        out.writeString(CARD_CORP_CD);
        out.writeString(CARD_NO);
    }
}

