package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_MYCD_MBL_P004_RES_REC extends TxMessage {
    private TX_MYCD_MBL_P004_RES_REC_DATA mTxKeyData;


    public TX_MYCD_MBL_P004_RES_REC(Object object) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_P004_RES_REC_DATA();
        super.initRecvMessage(object);
    }

    public String getRCPT_RFS_HPNO() throws JSONException {
        return getString(mTxKeyData.RCPT_USER_HPNO);
    }

    public String getRCPT_RFS_NM() throws JSONException {
        return getString(mTxKeyData.RCPT_USER_NM);
    }

    public String getRCPT_RFS_USER_NO() throws JSONException {
        return getString(mTxKeyData.RCPT_RFS_USER_NO);
    }

    public String getRCPT_USER_BSNN_NM() throws JSONException {
        return getString(mTxKeyData.RCPT_USER_BSNN_NM);
    }

    public String getRCPT_USER_PSN_NM() throws JSONException {
        return getString(mTxKeyData.RCPT_USER_PSN_NM);
    }

    public String getRCPT_USER_PSN_IMG() throws JSONException {
        return getString(mTxKeyData.RCPT_USER_PSN_IMG);
    }
    private class TX_MYCD_MBL_P004_RES_REC_DATA {

        private String RCPT_USER_HPNO = "RCPT_USER_HPNO";
        private String RCPT_USER_NM = "RCPT_USER_NM";
        private String RCPT_RFS_USER_NO = "RCPT_USER_NO";
        private String RCPT_USER_BSNN_NM = "RCPT_USER_BSNN_NM";
        private String RCPT_USER_PSN_NM = "RCPT_USER_PSN_NM";
        private String RCPT_USER_PSN_IMG = "RCPT_USER_PSN_IMG";
    }
}
