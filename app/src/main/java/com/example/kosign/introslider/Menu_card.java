package com.example.kosign.introslider;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.example.kosign.introslider.contran.Constants;
import com.example.kosign.introslider.carouselview.MyPageFragment;
import com.example.kosign.introslider.carouselview.MyPageItem;
import com.example.kosign.introslider.directionalcarousel.CarouselPagerAdapter;
import com.example.kosign.introslider.directionalcarousel.CarouselViewPager;
import com.example.kosign.introslider.directionalcarousel.page.OnPageClickListener;

import com.example.kosign.introslider.extras.Extra_Card;
import com.example.kosign.introslider.tran.ComTran;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L001_REQ;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L001_RES;
import com.example.kosign.introslider.tx.TX_MYCD_MBL_L001_RES_REC;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Menu_card extends AppCompatActivity implements OnPageClickListener<MyPageItem>,ComTran.OnComTranListener, ViewPager.OnPageChangeListener{

    private CarouselPagerAdapter<MyPageItem> mPagerAdapter;
    private ViewPager mViewPager;
    public static ArrayList<MyPageItem> mItems;
    private ComTran mComTran;
    private FrameLayout fl_has_card;
    private static Menu_card instance = null;
    private final String CARD_NO     = "CARD_NO";
    private final String CARD_CORP_CD   = "CARD_CORP_CD";
    private int cardSeletedIdx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a006_1_mycard);

        mViewPager = (CarouselViewPager) findViewById(R.id.carousel_pager);
        fl_has_card = (FrameLayout)findViewById(R.id.rl_has_card);

        mItems = new ArrayList<>();
        mComTran = new ComTran(this, this);
        mViewPager = (CarouselViewPager)findViewById(R.id.carousel_pager);
        mViewPager.addOnPageChangeListener(this);

        loadCardList();
    }

    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try{
            Constants.MainInfo.PUSH_GCM = false;
            Constants.MycardInfo.RELOAD_CARD = true;

            if(tranCode.equals(TX_MYCD_MBL_L001_REQ.TXNO)) {
                TX_MYCD_MBL_L001_RES res = new TX_MYCD_MBL_L001_RES(object);

                String CARD_MAGR_NM = res.getCARD_MAGR_NM();

                Constants.MainInfo.selected_card_manager_name = res.getCARD_MAGR_NM();

                TX_MYCD_MBL_L001_RES_REC rec = res.getCARD_LIST();

                mItems.clear();


                for(int i=0;i < rec.getLength();i++){
                    rec.move(i);
                    MyPageItem item = new MyPageItem();
                    item.setBIZ_NM(rec.getBIZ_NM());
                    item.setCARD_CORP_CD(rec.getCARD_CORP_CD());
                    Log.d("cardRes","data: "+rec.getCARD_CORP_CD());
                    item.setCARD_NICK_NM(rec.getCARD_NICK_NM());
                    item.setCARD_NO(rec.getCARD_NO());
                    item.setCARD_ORG_NM(rec.getCARD_ORG_NM());
                    item.setEXPR_TRM(rec.getEXPR_TRM());
                    mItems.add(item);

                }
            }

        }catch (Exception e){

        }

        mPagerAdapter = new CarouselPagerAdapter<>(getSupportFragmentManager(), MyPageFragment.class, R.layout.page_layout, mItems);
        mViewPager.setAdapter(mPagerAdapter);
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

    private void loadCardList() {
        try{
            Constants.MainInfo.PUSH_GCM = false;
            TX_MYCD_MBL_L001_REQ req = new TX_MYCD_MBL_L001_REQ();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
            req.setINQ_DT(df.format(c.getTime()));

            mComTran.requestData(TX_MYCD_MBL_L001_REQ.TXNO, req.getSendMessage(), true);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        Log.d("test", "pageSelectd: "+position);
        cardSeletedIdx = position;

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSingleTap(View view, MyPageItem item) {

    }

    @Override
    public void onDoubleTap(View view, MyPageItem item) {

    }



    public void click_card(View view) {

        Intent i=new Intent(getBaseContext(),AddEmployeeActivity.class);
        MyPageItem item=new MyPageItem();
        item.setCARD_NO(mItems.get(cardSeletedIdx).getCARD_NO());
        item.setCARD_CORP_CD(mItems.get(cardSeletedIdx).getCARD_CORP_CD());
        i.putExtra("CARD_CD", item);
        Log.d("CARD_CORP_CD","data: "+CARD_CORP_CD +", : "+cardSeletedIdx);
        startActivity(i);
    }


}