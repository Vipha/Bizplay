package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;
import org.json.JSONException;

/*
 * Created by KOSIGN on 10/5/2017.
 */

public class TX_MYCD_MBL_P001_REQ extends TxMessage {

    public static final String TXNO= "MYCD_MBL_P007";  //លេខកូដ user login
    private TX_MYCD_MBL_P001_REQ_DATA mTxKeyData;       //field

    public TX_MYCD_MBL_P001_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_P001_REQ_DATA();
        super.initSendMessage();
    }


    // REQ user ID
    public void setUSER_ID(String value)throws JSONException {
        mSendMessage.put(mTxKeyData.USER_ID,value);
    }

    // REQ user PW
    public void setUSER_PW(String value)throws JSONException {
        mSendMessage.put(mTxKeyData.USER_PW,value);
    }

    //data fields
    private class TX_MYCD_MBL_P001_REQ_DATA {
        private String USER_ID= "USER_ID";
        private String USER_PW= "USER_PW";
    }
}
