package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * cardlist response record
 * Created by Bunna on 0001, 01/07/2016.
 */
public class TX_MYCD_MBL_L001_RES_REC extends TxMessage {

    private TX_MYCD_MBL_L001_RES_REC_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_L001_RES_REC(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L001_RES_REC_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 카드사코드
     * @throws Exception
     */
    public String getCARD_CORP_CD() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_CORP_CD);
    }

    /**
     * 카드번호
     * @throws Exception
     */
    public String getCARD_NO() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_NO);
    }


    /**
     * 카드사명
     * @throws Exception
     */
    public String getCARD_ORG_NM() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_ORG_NM);
    }

    /**
     * 카드사업자명
     * @throws Exception
     */
    public String getBIZ_NM() throws JSONException,Exception{
        return getString(mTxKeyData.BIZ_NM);
    }

    /**
     * 카드별칭
     * @throws Exception
     */
    public String getCARD_NICK_NM() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_NICK_NM);
    }

    /**
     * MM/YY -> 카드유효기간
     * @throws Exception
     */
    public String getEXPR_TRM() throws JSONException,Exception{
        return getString(mTxKeyData.EXPR_TRM);
    }

    /**
     * 카드구분
     * @throws Exception
     */
    public String getCARD_GB() throws JSONException,Exception{
        return getString(mTxKeyData.CARD_GB);
    }

    /**
     * 스크래핑여부
     * @throws Exception
     */
    public String getSCRP_YN() throws JSONException,Exception{
        return getString(mTxKeyData.SCRP_YN);
    }

    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L001_RES_REC_DATA {
        private String CARD_CORP_CD			    = 	"CARD_CORP_CD";					//카드사코드
        private String CARD_NO		            = 	"CARD_NO";					//카드번호
        private String CARD_ORG_NM		        =    "CARD_ORG_NM";  					//카드사명
        private String BIZ_NM			        =    "BIZ_NM";  					//카드사업자명
        private String CARD_NICK_NM		        =    "CARD_NICK_NM";  					//카드별칭
        private String EXPR_TRM                 =    "EXPR_TRM";                        //MM/YY -> 카드유효기간
        private String CARD_GB                 =    "CARD_GB";                        //카드구분
        private String SCRP_YN                 =    "SCRP_YN";                        //스크래핑여부
    }

}
