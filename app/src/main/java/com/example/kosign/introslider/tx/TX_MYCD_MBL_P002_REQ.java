package com.example.kosign.introslider.tx;
import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/*
 * Created by KOSIGN on 10/5/2017.
 */

public class TX_MYCD_MBL_P002_REQ extends TxMessage {
    public static final String TXNO ="MYCD_MBL_P002";  // user logout
    private TX_MYCD_MBL_P002_REQ_DATA mtxKeyData;

    public TX_MYCD_MBL_P002_REQ() throws Exception {
        mtxKeyData  = new TX_MYCD_MBL_P002_REQ_DATA();
        super.initSendMessage();
    }
    public void setUSER_ID(String value) throws JSONException {
        mSendMessage.put(mtxKeyData.USER_ID, value);
    }

    private class TX_MYCD_MBL_P002_REQ_DATA {

        private String USER_ID="USER_ID";
    }
}
