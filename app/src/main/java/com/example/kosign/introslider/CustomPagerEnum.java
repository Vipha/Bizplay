package com.example.kosign.introslider;

/**
 * Created by mac on 10/2/17.
 */

public enum CustomPagerEnum {
    RED(R.string.slide_title, R.layout.a006_1_mycard),
    Plouk(R.string.slide_title2, R.layout.a006_2_receipt),
    ORANGE(R.string.slide_desc_1, R.layout.activity_invoice__report);

    private int mTitleResId;
    private int mLayoutResId;

    CustomPagerEnum(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}
