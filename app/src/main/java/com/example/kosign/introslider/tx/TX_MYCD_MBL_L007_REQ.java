package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/*
 * Created by User on 14-Feb-17.
 */

public class TX_MYCD_MBL_L007_REQ extends TxMessage {
    public static final String TXNO = "MYCD_MBL_L007";
    private TX_MYCD_MBL_L007_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_L007_REQ() throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L007_REQ_DATA();
        super.initSendMessage();
    }

    public void setCARD_CORP_CD(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, value);
    }

    public void setCARD_NO(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.CARD_NO, value);
    }
    private class TX_MYCD_MBL_L007_REQ_DATA{
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_NO = "CARD_NO";
    }
}
