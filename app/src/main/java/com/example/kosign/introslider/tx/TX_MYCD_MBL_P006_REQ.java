package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_MYCD_MBL_P006_REQ extends TxMessage {

    public static final String TXNO =   "MYCD_MBL_P006";    //Card authorization approval registration

    private TX_MYCD_MBL_P006_REQ_DATA mTxKeyData;
    public TX_MYCD_MBL_P006_REQ()throws Exception{
        mTxKeyData =new TX_MYCD_MBL_P006_REQ_DATA();
        super.initSendMessage();

    }

    private class TX_MYCD_MBL_P006_REQ_DATA {

        private String CD_USER_NO   =   "CD_USER_NO";
        private String SEND_DTM =    "SEND_DTM";
        private String NOTI_SEQ =   "NOTI_SEQ";

    }
}
