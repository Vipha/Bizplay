package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

/**
 * receipt list response record
 * Created by bunna on 0001, 01/07/2016.
 */
public class TX_MYCD_MBL_L002_RES_REC extends TxMessage {

    private TX_MYCD_MBL_L002_RES_REC_DATA mTxKeyData;		// 전문명세 필드

    public TX_MYCD_MBL_L002_RES_REC(Object obj) throws Exception {
        mTxKeyData = new TX_MYCD_MBL_L002_RES_REC_DATA();
        super.initRecvMessage(obj);
    }

    /**
     * 카드번호
     * @throws Exception
     */
    public String getCARD_NO() throws JSONException,Exception {
        return getString(mTxKeyData.CARD_NO);
    }

    /**
     * 승인일자
     * @throws Exception
     */
    public String getAPV_DT() throws JSONException,Exception {
        return getString(mTxKeyData.APV_DT);
    }

    /**
     * 일련번호
     * @throws Exception
     */
    public String getAPV_SEQ() throws JSONException,Exception {
        return getString(mTxKeyData.APV_SEQ);
    }

    /**
     * 승인시각
     * @throws Exception
     */
    public String getAPV_TM() throws JSONException,Exception {
        return getString(mTxKeyData.APV_TM);
    }
    /**
     * 승인번호
     * @throws Exception
     */
    public String getAPV_NO() throws JSONException,Exception {
        return getString(mTxKeyData.APV_NO);
    }

    /**
     * 승인금액
     * @throws Exception
     */
    public String getAPV_AMT() throws JSONException,Exception {
        return getString(mTxKeyData.APV_AMT);
    }
    /**
     * 승인취소여부
     * @throws Exception
     */
    public String getAPV_CAN_YN() throws JSONException,Exception {
        return getString(mTxKeyData.APV_CAN_YN);
    }

    /**
     * 승인취소일자
     * @throws Exception
     */
    public String getAPV_CAN_DT() throws JSONException,Exception {
        return getString(mTxKeyData.APV_CAN_DT);
    }
    /**
     * 할부개월수
     * @throws Exception
     */
    public String getITLM_MMS_CNT() throws JSONException,Exception {
        return getString(mTxKeyData.ITLM_MMS_CNT);
    }

    /**
     * 결제예정일자
     * @throws Exception
     */
    public String getSETL_SCHE_DT() throws JSONException,Exception {
        return getString(mTxKeyData.SETL_SCHE_DT);
    }
    /**
     * 가맹점명
     * @throws Exception
     */
    public String getMEST_NM() throws JSONException,Exception {
        return getString(mTxKeyData.MEST_NM);
    }

    /**
     * 가맹점번호
     * @throws Exception
     */
    public String getMEST_NO() throws JSONException,Exception {
        return getString(mTxKeyData.MEST_NO);
    }
    /**
     * 가맹점사업자번호
     * @throws Exception
     */
    public String getMEST_BIZ_NO() throws JSONException,Exception {
        return getString(mTxKeyData.MEST_BIZ_NO);
    }

    /**
     * 영수증등록 거래상태
     * @throws Exception
     */
    public String getRCPT_TX_STS() throws JSONException,Exception {
        return getString(mTxKeyData.RCPT_TX_STS);
    }
    /**
     * 영수증승인 거부여부
     * @throws Exception
     */
    public String getRCPT_RFS_STS() throws JSONException,Exception {
        return getString(mTxKeyData.RCPT_RFS_STS);
    }

    /**
     * 카드별칭
     * @throws Exception
     */
    public String getCARD_NICK_NM() throws JSONException,Exception {
        return getString(mTxKeyData.CARD_NICK_NM);
    }
    /**
     * 요일
     * @throws Exception
     */
    public String getDAY_NM() throws JSONException,Exception {
        return getString(mTxKeyData.DAY_NM);
    }

    /**
     * 용도코드
     * @throws Exception
     */
    public String getTRAN_KIND_CD() throws JSONException,Exception {
        return getString(mTxKeyData.TRAN_KIND_CD);
    }

    /**
     * 용도명
     * @throws Exception
     */
    public String getTRAN_KIND_NM() throws JSONException,Exception {
        return getString(mTxKeyData.TRAN_KIND_NM);
    }
    /**
     * 전문 Data 필드 설정
     */
    private class TX_MYCD_MBL_L002_RES_REC_DATA {
        private String CARD_NO			        = 	"CARD_NO";					//카드번호
        private String APV_DT			        = 	"APV_DT";					//승인일자
        private String APV_SEQ			        = 	"APV_SEQ";					//일련번호
        private String APV_TM			        = 	"APV_TM";					//승인시각
        private String APV_NO			        = 	"APV_NO";					//승인번호
        private String APV_AMT			        = 	"APV_AMT";					//승인금액
        private String APV_CAN_YN			        = 	"APV_CAN_YN";					//승인취소여부
        private String APV_CAN_DT			        = 	"APV_CAN_DT";					//승인취소일자
        private String ITLM_MMS_CNT			        = 	"ITLM_MMS_CNT";					//할부개월수
        private String SETL_SCHE_DT			        = 	"SETL_SCHE_DT";					//결제예정일자
        private String MEST_NM			        = 	"MEST_NM";					//가맹점명
        private String MEST_NO			        = 	"MEST_NO";					//가맹점번호
        private String MEST_BIZ_NO			        = 	"MEST_BIZ_NO";					//가맹점사업자번호
        private String RCPT_TX_STS			        = 	"RCPT_TX_STS";					//영수증등록 거래상태
        private String RCPT_RFS_STS			        = 	"RCPT_RFS_STS";					//영수증승인 거부여부
        private String CARD_NICK_NM			        = 	"CARD_NICK_NM";					//카드별칭
        private String DAY_NM			        = 	"DAY_NM";					//요일
        private String TRAN_KIND_CD			        = 	"TRAN_KIND_CD";					//용도코드
        private String TRAN_KIND_NM			        = 	"TRAN_KIND_NM";					//용도명

    }
}
