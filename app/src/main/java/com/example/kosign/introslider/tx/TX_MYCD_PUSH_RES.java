package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

public class TX_MYCD_PUSH_RES extends TxMessage {


	private TX_BIZCARD_PUSH_RES_DATA mTxKeyData;   // 전문명세 필드 

	public TX_MYCD_PUSH_RES(Object obj)throws Exception {

		mTxKeyData = new TX_BIZCARD_PUSH_RES_DATA();
		super.initRecvMessage(obj);
	}
	/**
	 *  등록 결과값
	 * @throws Exception
	 */
	public boolean isRESULT() throws JSONException {

		String strService = getString(mTxKeyData.C_RESULT);
		if(strService.equals("")) {
			return false;
		}
		return Boolean.parseBoolean(strService);
	}
	/**
	 * 전문 Data 필드 설정 
	 * @author nryoo
	 */
	private class TX_BIZCARD_PUSH_RES_DATA {

		private String C_RESULT= "_result";                  // 사용자아이디

	}






}
