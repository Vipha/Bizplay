package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

public class TX_MYCD_MBL_R001_REQ extends TxMessage {

    public static final String TXNO= "MYCD_MBL_R001";


    private TX_MYCD_MBL_R001_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_R001_REQ() throws Exception {

        mTxKeyData = new TX_MYCD_MBL_R001_REQ_DATA();
        super.initSendMessage();

    }

    public void setCARD_CORP_CD(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.CARD_CORP_CD, value);
    }


    public void setCARD_NO(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.CARD_NO, value);
    }
    public void setINQ_GB(String value) throws JSONException {
        mSendMessage.put(mTxKeyData.INQ_GB, value);
    }
    private class TX_MYCD_MBL_R001_REQ_DATA {
        private String CARD_CORP_CD	=	"CARD_CORP_CD";
        private String CARD_NO	    =	"CARD_NO";
        private String INQ_GB       = "INQ_GB";
    }
}
