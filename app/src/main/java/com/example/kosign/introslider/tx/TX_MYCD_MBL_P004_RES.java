package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

/**
 * Created by KOSIGN on 10/6/2017.
 */

public class TX_MYCD_MBL_P004_RES extends TxMessage {

    public static final String TXNO="MYCD_MBL_P004";
    private String RCPT_REC="RCPT_REC";
    public TX_MYCD_MBL_P004_RES(Object obj)throws Exception {
        super.initRecvMessage(obj);

    }

    public TX_MYCD_MBL_P004_RES_REC getRCPT_USER_REC() throws Exception {
        return new TX_MYCD_MBL_P004_RES_REC(getRecord(RCPT_REC));
    }

}
