package com.example.kosign.introslider.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

public class TX_MYCD_PUSH_REG_REQ extends TxMessage {
	
	public static final String TXNO = "MYCD_MBL_P003";
	private TX_BIZCARD_PUSH_REG_REQ_DATA mTxKeyData;	// 전문명세 필드 

	public TX_MYCD_PUSH_REG_REQ() throws Exception {
		mTxKeyData = new TX_BIZCARD_PUSH_REG_REQ_DATA();
		super.initSendMessage();
	}
	
	
	/**
	 * 푸시 서버 종류(APNS or GCM)
	 * @param value
	 * @throws Exception
	 */
	public void setPUSHSERVER_KIND(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_PUSHSERVER_KIND, value);
	}
	
	/**
	 *  앱 ID(패키지네임 or 번들아이덴티파이어)
	 * @param value
	 * @throws Exception
	 */
	public void setAPPID(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_APP_ID, value);
	}

	/**
	 * push reg id
	 * @param value
	 * @throws Exception
	 */
	public void setPUSH_ID(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_PUSH_ID, value);
	}
	
	/**
	 * 기기 모델명
	 * @param value
	 * @throws Exception
	 */
	public void setMODEL_NAME(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_MODEL_NAME, value);
	}

	/**
	 * 운영체제 버젼
	 * @param value
	 * @throws Exception
	 */
	public void setOS(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_OS, value);
	}

	
	/**
	 * 업체 ID
	 * @param value
	 * @throws Exception
	 */
	public void setCOMPANY_ID(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_COMPANY_ID, value);
	}
	
	
	/**
	 * 기기 ID(UDID값. 디바이스에 대한 유일한 값.)
	 * @param value
	 * @throws Exception
	 */
	public void setDEVICE_ID(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_DEVICE_ID, value);
	}
	
	/**
	 * 등록할 연계키
	 * @param value
	 * @throws Exception
	 */
	public void setRELATION_KEY(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_RELATION_KEY, value);
	}
	
	/**
	 * 재전송여부(Y or N)
	 * @param value
	 * @throws Exception
	 */
	public void setRETRANS_YN(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_RETRANS_YN, value);
	}

	
	/**
	 * 비고
	 * @param value
	 * @throws Exception
	 */
	public void setREMARK(String value) throws JSONException, Exception {
		mSendMessage.put(mTxKeyData.C_REMARK, value);
	}

	
//	/**
//	 * @param value
//	 * @throws Exception
//	 */
//	public void setWORKID(String value) throws JSONException, Exception {
//		mSendMessage.put(mTxKeyData.C_WORK_ID, value);
//	}
//	
//	public void setSENDERID(String value) throws JSONException, Exception {
//		mSendMessage.put(mTxKeyData.c_SENDER_CD, value);
//	}
//	
//	public void setUSERID(String value) throws JSONException, Exception {
//		mSendMessage.put(mTxKeyData.C_USER_ID, value);
//	}
	
	
	private class TX_BIZCARD_PUSH_REG_REQ_DATA {
		
		/*
				PUSHSERVER_KIND	
				APP_ID	
				PUSH_ID	
				MODEL_NAME	
				OS	
				COMPANY_ID	
				DEVICE_ID	
				RELATION_KEY	
				RETRANS_YN	
				REMARK	
		 */
		
		private String C_PUSHSERVER_KIND = "PUSHSERVER_KIND";	// 푸시 서버 종류(APNS or GCM)
		private String C_APP_ID = "APP_ID";					// 앱 ID(패키지네임 or 번들아이덴티파이어)
		private String C_PUSH_ID = "PUSH_ID";					// push reg id
		private String C_MODEL_NAME = "MODEL_NAME";			// 기기 모델명
		private String C_OS = "OS";							// 운영체제 버젼
		private String C_COMPANY_ID = "COMPANY_ID";			// 업체 ID
		private String C_DEVICE_ID = "DEVICE_ID";				// 기기 ID(UDID값. 디바이스에 대한 유일한 값.)
		private String C_RELATION_KEY = "RELATION_KEY";		//	등록할 연계키
		private String C_RETRANS_YN = "RETRANS_YN";			//	재전송여부(Y or N)
		private String C_REMARK = "REMARK";				//		비고
		
//		private String C_WORK_ID = "_work_id";
//		private String c_SENDER_CD = "_sender_cd";  2183 3820
//		private String C_USER_ID = "_user_id";


	}
	
}
